﻿namespace MusicCollectionGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.insertPersonButton = new System.Windows.Forms.Button();
            this.insertArtistButton = new System.Windows.Forms.Button();
            this.insertReleaseButton = new System.Windows.Forms.Button();
            this.dbConnectionGroupBoc = new System.Windows.Forms.GroupBox();
            this.clearDbFieldsButton = new System.Windows.Forms.Button();
            this.dbNameLabel = new System.Windows.Forms.Label();
            this.dbNameTextBox = new System.Windows.Forms.TextBox();
            this.dbPasswordLabel = new System.Windows.Forms.Label();
            this.dbAddressLabel = new System.Windows.Forms.Label();
            this.dbPortLabel = new System.Windows.Forms.Label();
            this.dbUsernameLabel = new System.Windows.Forms.Label();
            this.dbPasswordTextBox = new System.Windows.Forms.TextBox();
            this.dbUsernameTextBox = new System.Windows.Forms.TextBox();
            this.dbPortTextBox = new System.Windows.Forms.TextBox();
            this.dbAddressTextBox = new System.Windows.Forms.TextBox();
            this.testDbConnectionButton = new System.Windows.Forms.Button();
            this.dbToolsGroupBox = new System.Windows.Forms.GroupBox();
            this.dbToolsTabControl = new System.Windows.Forms.TabControl();
            this.browseTabPage = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.browseAlbumButton = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.insertTabPage = new System.Windows.Forms.TabPage();
            this.insertRecordCompanyButton = new System.Windows.Forms.Button();
            this.insertBoxsetButton = new System.Windows.Forms.Button();
            this.insertAlbumButton = new System.Windows.Forms.Button();
            this.deleteTabPage = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.updateTabPage = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.updatePersonButton = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.updateAlbumButton = new System.Windows.Forms.Button();
            this.updateReleaseButton = new System.Windows.Forms.Button();
            this.messagesGroupBox = new System.Windows.Forms.GroupBox();
            this.messagesTextBox = new System.Windows.Forms.TextBox();
            this.dbConnectionGroupBoc.SuspendLayout();
            this.dbToolsGroupBox.SuspendLayout();
            this.dbToolsTabControl.SuspendLayout();
            this.browseTabPage.SuspendLayout();
            this.insertTabPage.SuspendLayout();
            this.deleteTabPage.SuspendLayout();
            this.updateTabPage.SuspendLayout();
            this.messagesGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // insertPersonButton
            // 
            this.insertPersonButton.Location = new System.Drawing.Point(6, 7);
            this.insertPersonButton.Name = "insertPersonButton";
            this.insertPersonButton.Size = new System.Drawing.Size(80, 23);
            this.insertPersonButton.TabIndex = 4;
            this.insertPersonButton.Text = "Person";
            this.insertPersonButton.UseVisualStyleBackColor = true;
            this.insertPersonButton.Click += new System.EventHandler(this.addPersonButton_Click);
            // 
            // insertArtistButton
            // 
            this.insertArtistButton.Location = new System.Drawing.Point(91, 7);
            this.insertArtistButton.Name = "insertArtistButton";
            this.insertArtistButton.Size = new System.Drawing.Size(80, 23);
            this.insertArtistButton.TabIndex = 5;
            this.insertArtistButton.Text = "Artist";
            this.insertArtistButton.UseVisualStyleBackColor = true;
            this.insertArtistButton.Click += new System.EventHandler(this.addArtistButton_Click);
            // 
            // insertReleaseButton
            // 
            this.insertReleaseButton.Location = new System.Drawing.Point(91, 36);
            this.insertReleaseButton.Name = "insertReleaseButton";
            this.insertReleaseButton.Size = new System.Drawing.Size(80, 23);
            this.insertReleaseButton.TabIndex = 6;
            this.insertReleaseButton.Text = "Release";
            this.insertReleaseButton.UseVisualStyleBackColor = true;
            this.insertReleaseButton.Click += new System.EventHandler(this.addReleaseButton_Click);
            // 
            // dbConnectionGroupBoc
            // 
            this.dbConnectionGroupBoc.Controls.Add(this.clearDbFieldsButton);
            this.dbConnectionGroupBoc.Controls.Add(this.dbNameLabel);
            this.dbConnectionGroupBoc.Controls.Add(this.dbNameTextBox);
            this.dbConnectionGroupBoc.Controls.Add(this.dbPasswordLabel);
            this.dbConnectionGroupBoc.Controls.Add(this.dbAddressLabel);
            this.dbConnectionGroupBoc.Controls.Add(this.dbPortLabel);
            this.dbConnectionGroupBoc.Controls.Add(this.dbUsernameLabel);
            this.dbConnectionGroupBoc.Controls.Add(this.dbPasswordTextBox);
            this.dbConnectionGroupBoc.Controls.Add(this.dbUsernameTextBox);
            this.dbConnectionGroupBoc.Controls.Add(this.dbPortTextBox);
            this.dbConnectionGroupBoc.Controls.Add(this.dbAddressTextBox);
            this.dbConnectionGroupBoc.Controls.Add(this.testDbConnectionButton);
            this.dbConnectionGroupBoc.Location = new System.Drawing.Point(12, 12);
            this.dbConnectionGroupBoc.Name = "dbConnectionGroupBoc";
            this.dbConnectionGroupBoc.Size = new System.Drawing.Size(181, 178);
            this.dbConnectionGroupBoc.TabIndex = 7;
            this.dbConnectionGroupBoc.TabStop = false;
            this.dbConnectionGroupBoc.Text = "DB connection";
            // 
            // clearDbFieldsButton
            // 
            this.clearDbFieldsButton.Location = new System.Drawing.Point(94, 149);
            this.clearDbFieldsButton.Name = "clearDbFieldsButton";
            this.clearDbFieldsButton.Size = new System.Drawing.Size(81, 23);
            this.clearDbFieldsButton.TabIndex = 18;
            this.clearDbFieldsButton.Text = "Clear";
            this.clearDbFieldsButton.UseVisualStyleBackColor = true;
            this.clearDbFieldsButton.Click += new System.EventHandler(this.clearDbFieldsButton_Click);
            // 
            // dbNameLabel
            // 
            this.dbNameLabel.AutoSize = true;
            this.dbNameLabel.Location = new System.Drawing.Point(7, 126);
            this.dbNameLabel.Name = "dbNameLabel";
            this.dbNameLabel.Size = new System.Drawing.Size(51, 13);
            this.dbNameLabel.TabIndex = 17;
            this.dbNameLabel.Text = "DB name";
            // 
            // dbNameTextBox
            // 
            this.dbNameTextBox.Location = new System.Drawing.Point(69, 123);
            this.dbNameTextBox.Name = "dbNameTextBox";
            this.dbNameTextBox.Size = new System.Drawing.Size(106, 20);
            this.dbNameTextBox.TabIndex = 16;
            this.dbNameTextBox.Text = "musiccollection";
            // 
            // dbPasswordLabel
            // 
            this.dbPasswordLabel.AutoSize = true;
            this.dbPasswordLabel.Location = new System.Drawing.Point(6, 100);
            this.dbPasswordLabel.Name = "dbPasswordLabel";
            this.dbPasswordLabel.Size = new System.Drawing.Size(53, 13);
            this.dbPasswordLabel.TabIndex = 15;
            this.dbPasswordLabel.Text = "Password";
            // 
            // dbAddressLabel
            // 
            this.dbAddressLabel.AutoSize = true;
            this.dbAddressLabel.Location = new System.Drawing.Point(6, 22);
            this.dbAddressLabel.Name = "dbAddressLabel";
            this.dbAddressLabel.Size = new System.Drawing.Size(45, 13);
            this.dbAddressLabel.TabIndex = 14;
            this.dbAddressLabel.Text = "Address";
            // 
            // dbPortLabel
            // 
            this.dbPortLabel.AutoSize = true;
            this.dbPortLabel.Location = new System.Drawing.Point(7, 48);
            this.dbPortLabel.Name = "dbPortLabel";
            this.dbPortLabel.Size = new System.Drawing.Size(26, 13);
            this.dbPortLabel.TabIndex = 13;
            this.dbPortLabel.Text = "Port";
            // 
            // dbUsernameLabel
            // 
            this.dbUsernameLabel.AutoSize = true;
            this.dbUsernameLabel.Location = new System.Drawing.Point(6, 74);
            this.dbUsernameLabel.Name = "dbUsernameLabel";
            this.dbUsernameLabel.Size = new System.Drawing.Size(55, 13);
            this.dbUsernameLabel.TabIndex = 12;
            this.dbUsernameLabel.Text = "Username";
            // 
            // dbPasswordTextBox
            // 
            this.dbPasswordTextBox.Location = new System.Drawing.Point(69, 97);
            this.dbPasswordTextBox.Name = "dbPasswordTextBox";
            this.dbPasswordTextBox.PasswordChar = '*';
            this.dbPasswordTextBox.Size = new System.Drawing.Size(106, 20);
            this.dbPasswordTextBox.TabIndex = 11;
            // 
            // dbUsernameTextBox
            // 
            this.dbUsernameTextBox.Location = new System.Drawing.Point(69, 71);
            this.dbUsernameTextBox.Name = "dbUsernameTextBox";
            this.dbUsernameTextBox.Size = new System.Drawing.Size(106, 20);
            this.dbUsernameTextBox.TabIndex = 10;
            // 
            // dbPortTextBox
            // 
            this.dbPortTextBox.Location = new System.Drawing.Point(69, 45);
            this.dbPortTextBox.Name = "dbPortTextBox";
            this.dbPortTextBox.Size = new System.Drawing.Size(106, 20);
            this.dbPortTextBox.TabIndex = 9;
            this.dbPortTextBox.Text = "3306";
            // 
            // dbAddressTextBox
            // 
            this.dbAddressTextBox.Location = new System.Drawing.Point(69, 19);
            this.dbAddressTextBox.Name = "dbAddressTextBox";
            this.dbAddressTextBox.Size = new System.Drawing.Size(106, 20);
            this.dbAddressTextBox.TabIndex = 8;
            this.dbAddressTextBox.Text = "127.0.0.1";
            // 
            // testDbConnectionButton
            // 
            this.testDbConnectionButton.Location = new System.Drawing.Point(6, 149);
            this.testDbConnectionButton.Name = "testDbConnectionButton";
            this.testDbConnectionButton.Size = new System.Drawing.Size(81, 23);
            this.testDbConnectionButton.TabIndex = 0;
            this.testDbConnectionButton.Text = "Test connection";
            this.testDbConnectionButton.UseVisualStyleBackColor = true;
            this.testDbConnectionButton.Click += new System.EventHandler(this.testDbConnectionButton_Click);
            // 
            // dbToolsGroupBox
            // 
            this.dbToolsGroupBox.Controls.Add(this.dbToolsTabControl);
            this.dbToolsGroupBox.Location = new System.Drawing.Point(199, 12);
            this.dbToolsGroupBox.Name = "dbToolsGroupBox";
            this.dbToolsGroupBox.Size = new System.Drawing.Size(194, 152);
            this.dbToolsGroupBox.TabIndex = 8;
            this.dbToolsGroupBox.TabStop = false;
            this.dbToolsGroupBox.Text = "DB tools";
            // 
            // dbToolsTabControl
            // 
            this.dbToolsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dbToolsTabControl.Controls.Add(this.browseTabPage);
            this.dbToolsTabControl.Controls.Add(this.insertTabPage);
            this.dbToolsTabControl.Controls.Add(this.deleteTabPage);
            this.dbToolsTabControl.Controls.Add(this.updateTabPage);
            this.dbToolsTabControl.Location = new System.Drawing.Point(6, 19);
            this.dbToolsTabControl.Name = "dbToolsTabControl";
            this.dbToolsTabControl.SelectedIndex = 0;
            this.dbToolsTabControl.Size = new System.Drawing.Size(183, 127);
            this.dbToolsTabControl.TabIndex = 9;
            // 
            // browseTabPage
            // 
            this.browseTabPage.Controls.Add(this.button1);
            this.browseTabPage.Controls.Add(this.button2);
            this.browseTabPage.Controls.Add(this.button3);
            this.browseTabPage.Controls.Add(this.button4);
            this.browseTabPage.Controls.Add(this.browseAlbumButton);
            this.browseTabPage.Controls.Add(this.button6);
            this.browseTabPage.Location = new System.Drawing.Point(4, 22);
            this.browseTabPage.Name = "browseTabPage";
            this.browseTabPage.Size = new System.Drawing.Size(175, 101);
            this.browseTabPage.TabIndex = 3;
            this.browseTabPage.Text = "Browse";
            this.browseTabPage.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(91, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Rec. comp.";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Person";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 65);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 23);
            this.button3.TabIndex = 14;
            this.button3.Text = "Boxset";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(91, 7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Artist";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // browseAlbumButton
            // 
            this.browseAlbumButton.Location = new System.Drawing.Point(6, 36);
            this.browseAlbumButton.Name = "browseAlbumButton";
            this.browseAlbumButton.Size = new System.Drawing.Size(80, 23);
            this.browseAlbumButton.TabIndex = 13;
            this.browseAlbumButton.Text = "Album";
            this.browseAlbumButton.UseVisualStyleBackColor = true;
            this.browseAlbumButton.Click += new System.EventHandler(this.browseAlbumButton_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(91, 36);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(80, 23);
            this.button6.TabIndex = 12;
            this.button6.Text = "Release";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // insertTabPage
            // 
            this.insertTabPage.Controls.Add(this.insertRecordCompanyButton);
            this.insertTabPage.Controls.Add(this.insertPersonButton);
            this.insertTabPage.Controls.Add(this.insertBoxsetButton);
            this.insertTabPage.Controls.Add(this.insertArtistButton);
            this.insertTabPage.Controls.Add(this.insertAlbumButton);
            this.insertTabPage.Controls.Add(this.insertReleaseButton);
            this.insertTabPage.Location = new System.Drawing.Point(4, 22);
            this.insertTabPage.Name = "insertTabPage";
            this.insertTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.insertTabPage.Size = new System.Drawing.Size(175, 101);
            this.insertTabPage.TabIndex = 0;
            this.insertTabPage.Text = "Insert";
            this.insertTabPage.UseVisualStyleBackColor = true;
            // 
            // insertRecordCompanyButton
            // 
            this.insertRecordCompanyButton.Location = new System.Drawing.Point(91, 65);
            this.insertRecordCompanyButton.Name = "insertRecordCompanyButton";
            this.insertRecordCompanyButton.Size = new System.Drawing.Size(80, 23);
            this.insertRecordCompanyButton.TabIndex = 9;
            this.insertRecordCompanyButton.Text = "Rec. comp.";
            this.insertRecordCompanyButton.UseVisualStyleBackColor = true;
            this.insertRecordCompanyButton.Click += new System.EventHandler(this.insertRecordCompanyButton_Click);
            // 
            // insertBoxsetButton
            // 
            this.insertBoxsetButton.Location = new System.Drawing.Point(6, 65);
            this.insertBoxsetButton.Name = "insertBoxsetButton";
            this.insertBoxsetButton.Size = new System.Drawing.Size(80, 23);
            this.insertBoxsetButton.TabIndex = 8;
            this.insertBoxsetButton.Text = "Boxset";
            this.insertBoxsetButton.UseVisualStyleBackColor = true;
            this.insertBoxsetButton.Click += new System.EventHandler(this.insertBoxsetButton_Click);
            // 
            // insertAlbumButton
            // 
            this.insertAlbumButton.Location = new System.Drawing.Point(6, 36);
            this.insertAlbumButton.Name = "insertAlbumButton";
            this.insertAlbumButton.Size = new System.Drawing.Size(80, 23);
            this.insertAlbumButton.TabIndex = 7;
            this.insertAlbumButton.Text = "Album";
            this.insertAlbumButton.UseVisualStyleBackColor = true;
            this.insertAlbumButton.Click += new System.EventHandler(this.insertAlbumButton_Click);
            // 
            // deleteTabPage
            // 
            this.deleteTabPage.Controls.Add(this.label1);
            this.deleteTabPage.Location = new System.Drawing.Point(4, 22);
            this.deleteTabPage.Name = "deleteTabPage";
            this.deleteTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.deleteTabPage.Size = new System.Drawing.Size(175, 101);
            this.deleteTabPage.TabIndex = 1;
            this.deleteTabPage.Text = "Delete";
            this.deleteTabPage.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Not implemented";
            // 
            // updateTabPage
            // 
            this.updateTabPage.Controls.Add(this.button7);
            this.updateTabPage.Controls.Add(this.updatePersonButton);
            this.updateTabPage.Controls.Add(this.button9);
            this.updateTabPage.Controls.Add(this.button10);
            this.updateTabPage.Controls.Add(this.updateAlbumButton);
            this.updateTabPage.Controls.Add(this.updateReleaseButton);
            this.updateTabPage.Location = new System.Drawing.Point(4, 22);
            this.updateTabPage.Name = "updateTabPage";
            this.updateTabPage.Size = new System.Drawing.Size(175, 101);
            this.updateTabPage.TabIndex = 2;
            this.updateTabPage.Text = "Update";
            this.updateTabPage.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(91, 65);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 23);
            this.button7.TabIndex = 15;
            this.button7.Text = "Rec. comp.";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // updatePersonButton
            // 
            this.updatePersonButton.Location = new System.Drawing.Point(6, 7);
            this.updatePersonButton.Name = "updatePersonButton";
            this.updatePersonButton.Size = new System.Drawing.Size(80, 23);
            this.updatePersonButton.TabIndex = 10;
            this.updatePersonButton.Text = "Person";
            this.updatePersonButton.UseVisualStyleBackColor = true;
            this.updatePersonButton.Click += new System.EventHandler(this.updatePersonButton_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(6, 65);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(80, 23);
            this.button9.TabIndex = 14;
            this.button9.Text = "Boxset";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(91, 7);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(80, 23);
            this.button10.TabIndex = 11;
            this.button10.Text = "Artist";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // updateAlbumButton
            // 
            this.updateAlbumButton.Location = new System.Drawing.Point(6, 36);
            this.updateAlbumButton.Name = "updateAlbumButton";
            this.updateAlbumButton.Size = new System.Drawing.Size(80, 23);
            this.updateAlbumButton.TabIndex = 13;
            this.updateAlbumButton.Text = "Album";
            this.updateAlbumButton.UseVisualStyleBackColor = true;
            this.updateAlbumButton.Click += new System.EventHandler(this.updateAlbumButton_Click);
            // 
            // updateReleaseButton
            // 
            this.updateReleaseButton.Location = new System.Drawing.Point(91, 36);
            this.updateReleaseButton.Name = "updateReleaseButton";
            this.updateReleaseButton.Size = new System.Drawing.Size(80, 23);
            this.updateReleaseButton.TabIndex = 12;
            this.updateReleaseButton.Text = "Release";
            this.updateReleaseButton.UseVisualStyleBackColor = true;
            this.updateReleaseButton.Click += new System.EventHandler(this.updateReleaseButton_Click);
            // 
            // messagesGroupBox
            // 
            this.messagesGroupBox.Controls.Add(this.messagesTextBox);
            this.messagesGroupBox.Location = new System.Drawing.Point(12, 196);
            this.messagesGroupBox.Name = "messagesGroupBox";
            this.messagesGroupBox.Size = new System.Drawing.Size(381, 111);
            this.messagesGroupBox.TabIndex = 9;
            this.messagesGroupBox.TabStop = false;
            this.messagesGroupBox.Text = "Messages";
            // 
            // messagesTextBox
            // 
            this.messagesTextBox.Enabled = false;
            this.messagesTextBox.Location = new System.Drawing.Point(6, 19);
            this.messagesTextBox.Multiline = true;
            this.messagesTextBox.Name = "messagesTextBox";
            this.messagesTextBox.Size = new System.Drawing.Size(369, 86);
            this.messagesTextBox.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 319);
            this.Controls.Add(this.messagesGroupBox);
            this.Controls.Add(this.dbToolsGroupBox);
            this.Controls.Add(this.dbConnectionGroupBoc);
            this.Name = "Form1";
            this.Text = "Music DB tool";
            this.dbConnectionGroupBoc.ResumeLayout(false);
            this.dbConnectionGroupBoc.PerformLayout();
            this.dbToolsGroupBox.ResumeLayout(false);
            this.dbToolsTabControl.ResumeLayout(false);
            this.browseTabPage.ResumeLayout(false);
            this.insertTabPage.ResumeLayout(false);
            this.deleteTabPage.ResumeLayout(false);
            this.deleteTabPage.PerformLayout();
            this.updateTabPage.ResumeLayout(false);
            this.messagesGroupBox.ResumeLayout(false);
            this.messagesGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button insertPersonButton;
        private System.Windows.Forms.Button insertArtistButton;
        private System.Windows.Forms.Button insertReleaseButton;
        private System.Windows.Forms.GroupBox dbConnectionGroupBoc;
        private System.Windows.Forms.TextBox dbPasswordTextBox;
        private System.Windows.Forms.TextBox dbUsernameTextBox;
        private System.Windows.Forms.TextBox dbPortTextBox;
        private System.Windows.Forms.TextBox dbAddressTextBox;
        private System.Windows.Forms.Button testDbConnectionButton;
        private System.Windows.Forms.Label dbPasswordLabel;
        private System.Windows.Forms.Label dbAddressLabel;
        private System.Windows.Forms.Label dbPortLabel;
        private System.Windows.Forms.Label dbUsernameLabel;
        private System.Windows.Forms.GroupBox dbToolsGroupBox;
        private System.Windows.Forms.TabControl dbToolsTabControl;
        private System.Windows.Forms.TabPage insertTabPage;
        private System.Windows.Forms.Button insertBoxsetButton;
        private System.Windows.Forms.Button insertAlbumButton;
        private System.Windows.Forms.TabPage deleteTabPage;
        private System.Windows.Forms.TabPage updateTabPage;
        private System.Windows.Forms.Button insertRecordCompanyButton;
        private System.Windows.Forms.GroupBox messagesGroupBox;
        private System.Windows.Forms.TextBox messagesTextBox;
        private System.Windows.Forms.Button clearDbFieldsButton;
        private System.Windows.Forms.Label dbNameLabel;
        private System.Windows.Forms.TextBox dbNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage browseTabPage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button browseAlbumButton;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button updatePersonButton;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button updateAlbumButton;
        private System.Windows.Forms.Button updateReleaseButton;
    }
}

