﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollectionGUI
{
    public partial class Form1 : Form
    {
        //private MusicCollection.MusicCollection LoadedMusicCollection { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void addPersonButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot insert person to DB.";
                return;
            }

            try
            {
                NewPerson newPerson = new NewPerson(GenerateMySQLConnectionString());
                newPerson.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private void addArtistButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot insert artist to DB.";
                return;
            }

            try
            {
                NewArtist newArtist = new NewArtist(GenerateMySQLConnectionString());
                newArtist.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private void addReleaseButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot insert release to DB.";
                return;
            }

            try
            {
                NewRelease newRelease = new NewRelease(GenerateMySQLConnectionString());
                newRelease.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private string GenerateMySQLConnectionString()
        {
            string connString =
                "server=" + dbAddressTextBox.Text
                + ";user=" + dbUsernameTextBox.Text
                + ";database=" + dbNameTextBox.Text
                + ";port=" + dbPortTextBox.Text
                + ";password=" + dbPasswordTextBox.Text
                + ";AllowZeroDateTime=true";
            // TODO: check values!
            return connString;
        }

        private bool TestMySQLConnection()
        {
            try
            {
                using (MySqlConnection dbConnection = new MySqlConnection(GenerateMySQLConnectionString()))
                {
                    dbConnection.Open();
                    dbConnection.Close();
                }
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }

            // TODO: also check if all the required tables are present and correct
        }

        private void testDbConnectionButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
                messagesTextBox.Text = "MySQL connection test failed. Check credentials.";
            else
                messagesTextBox.Text = "MySQL connection test successful.";
        }

        private void insertAlbumButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot insert album to DB.";
                return;
            }

            try
            {
                NewAlbum newAlbum = new NewAlbum(GenerateMySQLConnectionString());
                newAlbum.ShowDialog();
            }
            catch (MySqlException ex)
            {
                messagesTextBox.Text = ex.Message;
            }
            // TODO: check dialogResult
        }

        private void insertRecordCompanyButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot insert record company to DB.";
                return;
            }

            try
            {
                NewRecordCompany newReccomp = new NewRecordCompany(GenerateMySQLConnectionString());
                newReccomp.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private void insertBoxsetButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot insert boxset to DB.";
                return;
            }

            try
            {
                NewBoxset newBoxset = new NewBoxset(GenerateMySQLConnectionString());
                newBoxset.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private void clearDbFieldsButton_Click(object sender, EventArgs e)
        {
            dbAddressTextBox.Text = "";
            dbNameTextBox.Text = "";
            dbPasswordTextBox.Text = "";
            dbUsernameTextBox.Text = "";
            dbPortTextBox.Text = "";
        }

        private void updateAlbumButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot edit album info.";
                return;
            }

            try
            {
                EditAlbum editAlbum = new EditAlbum(GenerateMySQLConnectionString());
                editAlbum.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private void updateReleaseButton_Click(object sender, EventArgs e)
        {

        }

        private void updatePersonButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot edit person info.";
                return;
            }

            try
            {
                EditPerson editPerson = new EditPerson(GenerateMySQLConnectionString());
                editPerson.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }

        private void browseAlbumButton_Click(object sender, EventArgs e)
        {
            if (!TestMySQLConnection())
            {
                messagesTextBox.Text = "Failed to connect to MySQL DB. Cannot browse album info.";
                return;
            }
            try
            {
                BrowseAlbums browseAlbums = new BrowseAlbums(GenerateMySQLConnectionString());
                browseAlbums.ShowDialog();
            }
            catch (MySqlException)
            {
                // TODO: add error message to running log
            }
            // TODO: check dialogResult
        }
    }
}
