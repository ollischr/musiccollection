﻿namespace MusicCollectionGUI
{
    partial class BrowseAlbums
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.artistListBox = new System.Windows.Forms.ListBox();
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.albumInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.albumInfoGeneralInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.albumInfoReleasesGroupBox = new System.Windows.Forms.GroupBox();
            this.releasesListBox = new System.Windows.Forms.ListBox();
            this.albumPersonnelGroupBox = new System.Windows.Forms.GroupBox();
            this.albumPersonnelTabControl = new System.Windows.Forms.TabControl();
            this.musiciansTabPage = new System.Windows.Forms.TabPage();
            this.albumMusiciansListBox = new System.Windows.Forms.ListBox();
            this.otherPersonnelTabPage = new System.Windows.Forms.TabPage();
            this.albumOtherPersonnelListBox = new System.Windows.Forms.ListBox();
            this.albumInfoYearGroupBox = new System.Windows.Forms.GroupBox();
            this.albumInfoYearTextBox = new System.Windows.Forms.TextBox();
            this.albumInfoTitleGroupBox = new System.Windows.Forms.GroupBox();
            this.albumInfoTitleTextBox = new System.Windows.Forms.TextBox();
            this.albumInfoArtistGroupBox = new System.Windows.Forms.GroupBox();
            this.albumInfoArtistTextBox = new System.Windows.Forms.TextBox();
            this.albumCoverGroupBox = new System.Windows.Forms.GroupBox();
            this.albumCoverPictureBox = new System.Windows.Forms.PictureBox();
            this.albumsGroupBox = new System.Windows.Forms.GroupBox();
            this.albumsListBox = new System.Windows.Forms.ListBox();
            this.filterGroupBox = new System.Windows.Forms.GroupBox();
            this.selectByArtistYearButton = new System.Windows.Forms.Button();
            this.yearGroupBox = new System.Windows.Forms.GroupBox();
            this.selectNoneYearsButton = new System.Windows.Forms.Button();
            this.yearListBox = new System.Windows.Forms.ListBox();
            this.selectAllYearsButton = new System.Windows.Forms.Button();
            this.artistGroupBox = new System.Windows.Forms.GroupBox();
            this.selectNoneArtistsButton = new System.Windows.Forms.Button();
            this.selectAllArtistsButton = new System.Windows.Forms.Button();
            this.personGroupBox = new System.Windows.Forms.GroupBox();
            this.personListBox = new System.Windows.Forms.ListBox();
            this.albumFilterModeTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.selectByPersonButton = new System.Windows.Forms.Button();
            this.clearPersonSelectionButton = new System.Windows.Forms.Button();
            this.mainGroupBox.SuspendLayout();
            this.albumInfoGroupBox.SuspendLayout();
            this.albumInfoGeneralInfoGroupBox.SuspendLayout();
            this.albumInfoReleasesGroupBox.SuspendLayout();
            this.albumPersonnelGroupBox.SuspendLayout();
            this.albumPersonnelTabControl.SuspendLayout();
            this.musiciansTabPage.SuspendLayout();
            this.otherPersonnelTabPage.SuspendLayout();
            this.albumInfoYearGroupBox.SuspendLayout();
            this.albumInfoTitleGroupBox.SuspendLayout();
            this.albumInfoArtistGroupBox.SuspendLayout();
            this.albumCoverGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.albumCoverPictureBox)).BeginInit();
            this.albumsGroupBox.SuspendLayout();
            this.filterGroupBox.SuspendLayout();
            this.yearGroupBox.SuspendLayout();
            this.artistGroupBox.SuspendLayout();
            this.personGroupBox.SuspendLayout();
            this.albumFilterModeTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // artistListBox
            // 
            this.artistListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.artistListBox.DisplayMember = "Name";
            this.artistListBox.FormattingEnabled = true;
            this.artistListBox.Location = new System.Drawing.Point(6, 19);
            this.artistListBox.Name = "artistListBox";
            this.artistListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.artistListBox.Size = new System.Drawing.Size(182, 147);
            this.artistListBox.TabIndex = 0;
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.albumInfoGroupBox);
            this.mainGroupBox.Controls.Add(this.albumsGroupBox);
            this.mainGroupBox.Controls.Add(this.filterGroupBox);
            this.mainGroupBox.Location = new System.Drawing.Point(12, 12);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Size = new System.Drawing.Size(905, 600);
            this.mainGroupBox.TabIndex = 1;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Browse albums";
            // 
            // albumInfoGroupBox
            // 
            this.albumInfoGroupBox.Controls.Add(this.albumInfoGeneralInfoGroupBox);
            this.albumInfoGroupBox.Controls.Add(this.albumCoverGroupBox);
            this.albumInfoGroupBox.Location = new System.Drawing.Point(354, 19);
            this.albumInfoGroupBox.Name = "albumInfoGroupBox";
            this.albumInfoGroupBox.Size = new System.Drawing.Size(545, 463);
            this.albumInfoGroupBox.TabIndex = 3;
            this.albumInfoGroupBox.TabStop = false;
            this.albumInfoGroupBox.Text = "Album info";
            // 
            // albumInfoGeneralInfoGroupBox
            // 
            this.albumInfoGeneralInfoGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.albumInfoGeneralInfoGroupBox.Controls.Add(this.albumInfoReleasesGroupBox);
            this.albumInfoGeneralInfoGroupBox.Controls.Add(this.albumPersonnelGroupBox);
            this.albumInfoGeneralInfoGroupBox.Controls.Add(this.albumInfoYearGroupBox);
            this.albumInfoGeneralInfoGroupBox.Controls.Add(this.albumInfoTitleGroupBox);
            this.albumInfoGeneralInfoGroupBox.Controls.Add(this.albumInfoArtistGroupBox);
            this.albumInfoGeneralInfoGroupBox.Location = new System.Drawing.Point(6, 19);
            this.albumInfoGeneralInfoGroupBox.Name = "albumInfoGeneralInfoGroupBox";
            this.albumInfoGeneralInfoGroupBox.Size = new System.Drawing.Size(415, 332);
            this.albumInfoGeneralInfoGroupBox.TabIndex = 5;
            this.albumInfoGeneralInfoGroupBox.TabStop = false;
            this.albumInfoGeneralInfoGroupBox.Text = "General info";
            // 
            // albumInfoReleasesGroupBox
            // 
            this.albumInfoReleasesGroupBox.Controls.Add(this.releasesListBox);
            this.albumInfoReleasesGroupBox.Location = new System.Drawing.Point(193, 121);
            this.albumInfoReleasesGroupBox.Name = "albumInfoReleasesGroupBox";
            this.albumInfoReleasesGroupBox.Size = new System.Drawing.Size(216, 205);
            this.albumInfoReleasesGroupBox.TabIndex = 16;
            this.albumInfoReleasesGroupBox.TabStop = false;
            this.albumInfoReleasesGroupBox.Text = "Releases";
            // 
            // releasesListBox
            // 
            this.releasesListBox.DisplayMember = "DisplayString";
            this.releasesListBox.FormattingEnabled = true;
            this.releasesListBox.Location = new System.Drawing.Point(5, 18);
            this.releasesListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.releasesListBox.Name = "releasesListBox";
            this.releasesListBox.Size = new System.Drawing.Size(208, 173);
            this.releasesListBox.TabIndex = 0;
            // 
            // albumPersonnelGroupBox
            // 
            this.albumPersonnelGroupBox.Controls.Add(this.albumPersonnelTabControl);
            this.albumPersonnelGroupBox.Location = new System.Drawing.Point(6, 121);
            this.albumPersonnelGroupBox.Name = "albumPersonnelGroupBox";
            this.albumPersonnelGroupBox.Size = new System.Drawing.Size(181, 205);
            this.albumPersonnelGroupBox.TabIndex = 0;
            this.albumPersonnelGroupBox.TabStop = false;
            this.albumPersonnelGroupBox.Text = "Personnel";
            // 
            // albumPersonnelTabControl
            // 
            this.albumPersonnelTabControl.Controls.Add(this.musiciansTabPage);
            this.albumPersonnelTabControl.Controls.Add(this.otherPersonnelTabPage);
            this.albumPersonnelTabControl.Location = new System.Drawing.Point(3, 16);
            this.albumPersonnelTabControl.Name = "albumPersonnelTabControl";
            this.albumPersonnelTabControl.SelectedIndex = 0;
            this.albumPersonnelTabControl.Size = new System.Drawing.Size(175, 183);
            this.albumPersonnelTabControl.TabIndex = 0;
            // 
            // musiciansTabPage
            // 
            this.musiciansTabPage.Controls.Add(this.albumMusiciansListBox);
            this.musiciansTabPage.Location = new System.Drawing.Point(4, 22);
            this.musiciansTabPage.Name = "musiciansTabPage";
            this.musiciansTabPage.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.musiciansTabPage.Size = new System.Drawing.Size(167, 157);
            this.musiciansTabPage.TabIndex = 0;
            this.musiciansTabPage.Text = "Musicians";
            this.musiciansTabPage.UseVisualStyleBackColor = true;
            // 
            // albumMusiciansListBox
            // 
            this.albumMusiciansListBox.DisplayMember = "FullName";
            this.albumMusiciansListBox.FormattingEnabled = true;
            this.albumMusiciansListBox.Location = new System.Drawing.Point(6, 6);
            this.albumMusiciansListBox.Name = "albumMusiciansListBox";
            this.albumMusiciansListBox.Size = new System.Drawing.Size(155, 147);
            this.albumMusiciansListBox.TabIndex = 6;
            // 
            // otherPersonnelTabPage
            // 
            this.otherPersonnelTabPage.Controls.Add(this.albumOtherPersonnelListBox);
            this.otherPersonnelTabPage.Location = new System.Drawing.Point(4, 22);
            this.otherPersonnelTabPage.Name = "otherPersonnelTabPage";
            this.otherPersonnelTabPage.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.otherPersonnelTabPage.Size = new System.Drawing.Size(167, 157);
            this.otherPersonnelTabPage.TabIndex = 1;
            this.otherPersonnelTabPage.Text = "Other personnel";
            this.otherPersonnelTabPage.UseVisualStyleBackColor = true;
            // 
            // albumOtherPersonnelListBox
            // 
            this.albumOtherPersonnelListBox.DisplayMember = "FullName";
            this.albumOtherPersonnelListBox.FormattingEnabled = true;
            this.albumOtherPersonnelListBox.Location = new System.Drawing.Point(6, 6);
            this.albumOtherPersonnelListBox.Name = "albumOtherPersonnelListBox";
            this.albumOtherPersonnelListBox.Size = new System.Drawing.Size(200, 95);
            this.albumOtherPersonnelListBox.TabIndex = 7;
            // 
            // albumInfoYearGroupBox
            // 
            this.albumInfoYearGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.albumInfoYearGroupBox.Controls.Add(this.albumInfoYearTextBox);
            this.albumInfoYearGroupBox.Location = new System.Drawing.Point(327, 19);
            this.albumInfoYearGroupBox.Name = "albumInfoYearGroupBox";
            this.albumInfoYearGroupBox.Size = new System.Drawing.Size(82, 45);
            this.albumInfoYearGroupBox.TabIndex = 15;
            this.albumInfoYearGroupBox.TabStop = false;
            this.albumInfoYearGroupBox.Text = "Year";
            // 
            // albumInfoYearTextBox
            // 
            this.albumInfoYearTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.albumInfoYearTextBox.Location = new System.Drawing.Point(6, 19);
            this.albumInfoYearTextBox.Name = "albumInfoYearTextBox";
            this.albumInfoYearTextBox.ReadOnly = true;
            this.albumInfoYearTextBox.Size = new System.Drawing.Size(70, 20);
            this.albumInfoYearTextBox.TabIndex = 9;
            // 
            // albumInfoTitleGroupBox
            // 
            this.albumInfoTitleGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.albumInfoTitleGroupBox.Controls.Add(this.albumInfoTitleTextBox);
            this.albumInfoTitleGroupBox.Location = new System.Drawing.Point(6, 70);
            this.albumInfoTitleGroupBox.Name = "albumInfoTitleGroupBox";
            this.albumInfoTitleGroupBox.Size = new System.Drawing.Size(403, 45);
            this.albumInfoTitleGroupBox.TabIndex = 13;
            this.albumInfoTitleGroupBox.TabStop = false;
            this.albumInfoTitleGroupBox.Text = "Title";
            // 
            // albumInfoTitleTextBox
            // 
            this.albumInfoTitleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.albumInfoTitleTextBox.Location = new System.Drawing.Point(6, 19);
            this.albumInfoTitleTextBox.Name = "albumInfoTitleTextBox";
            this.albumInfoTitleTextBox.ReadOnly = true;
            this.albumInfoTitleTextBox.Size = new System.Drawing.Size(391, 20);
            this.albumInfoTitleTextBox.TabIndex = 9;
            // 
            // albumInfoArtistGroupBox
            // 
            this.albumInfoArtistGroupBox.Controls.Add(this.albumInfoArtistTextBox);
            this.albumInfoArtistGroupBox.Location = new System.Drawing.Point(6, 19);
            this.albumInfoArtistGroupBox.Name = "albumInfoArtistGroupBox";
            this.albumInfoArtistGroupBox.Size = new System.Drawing.Size(315, 45);
            this.albumInfoArtistGroupBox.TabIndex = 12;
            this.albumInfoArtistGroupBox.TabStop = false;
            this.albumInfoArtistGroupBox.Text = "Artist";
            // 
            // albumInfoArtistTextBox
            // 
            this.albumInfoArtistTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.albumInfoArtistTextBox.Location = new System.Drawing.Point(6, 19);
            this.albumInfoArtistTextBox.Name = "albumInfoArtistTextBox";
            this.albumInfoArtistTextBox.ReadOnly = true;
            this.albumInfoArtistTextBox.Size = new System.Drawing.Size(303, 20);
            this.albumInfoArtistTextBox.TabIndex = 7;
            // 
            // albumCoverGroupBox
            // 
            this.albumCoverGroupBox.AutoSize = true;
            this.albumCoverGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.albumCoverGroupBox.Controls.Add(this.albumCoverPictureBox);
            this.albumCoverGroupBox.Location = new System.Drawing.Point(427, 19);
            this.albumCoverGroupBox.Name = "albumCoverGroupBox";
            this.albumCoverGroupBox.Size = new System.Drawing.Size(112, 138);
            this.albumCoverGroupBox.TabIndex = 0;
            this.albumCoverGroupBox.TabStop = false;
            this.albumCoverGroupBox.Text = "Cover art";
            // 
            // albumCoverPictureBox
            // 
            this.albumCoverPictureBox.Location = new System.Drawing.Point(6, 19);
            this.albumCoverPictureBox.Name = "albumCoverPictureBox";
            this.albumCoverPictureBox.Size = new System.Drawing.Size(100, 100);
            this.albumCoverPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.albumCoverPictureBox.TabIndex = 4;
            this.albumCoverPictureBox.TabStop = false;
            // 
            // albumsGroupBox
            // 
            this.albumsGroupBox.Controls.Add(this.albumsListBox);
            this.albumsGroupBox.Location = new System.Drawing.Point(6, 332);
            this.albumsGroupBox.Name = "albumsGroupBox";
            this.albumsGroupBox.Size = new System.Drawing.Size(342, 199);
            this.albumsGroupBox.TabIndex = 2;
            this.albumsGroupBox.TabStop = false;
            this.albumsGroupBox.Text = "Albums";
            // 
            // albumsListBox
            // 
            this.albumsListBox.DisplayMember = "Title";
            this.albumsListBox.FormattingEnabled = true;
            this.albumsListBox.Location = new System.Drawing.Point(6, 19);
            this.albumsListBox.Name = "albumsListBox";
            this.albumsListBox.Size = new System.Drawing.Size(330, 173);
            this.albumsListBox.TabIndex = 0;
            this.albumsListBox.SelectedIndexChanged += new System.EventHandler(this.albumsListBox_SelectedIndexChanged);
            // 
            // filterGroupBox
            // 
            this.filterGroupBox.Controls.Add(this.albumFilterModeTabControl);
            this.filterGroupBox.Location = new System.Drawing.Point(6, 19);
            this.filterGroupBox.Name = "filterGroupBox";
            this.filterGroupBox.Size = new System.Drawing.Size(342, 307);
            this.filterGroupBox.TabIndex = 2;
            this.filterGroupBox.TabStop = false;
            this.filterGroupBox.Text = "Filter values";
            // 
            // selectByArtistYearButton
            // 
            this.selectByArtistYearButton.Location = new System.Drawing.Point(6, 226);
            this.selectByArtistYearButton.Name = "selectByArtistYearButton";
            this.selectByArtistYearButton.Size = new System.Drawing.Size(75, 23);
            this.selectByArtistYearButton.TabIndex = 3;
            this.selectByArtistYearButton.Text = "Select";
            this.selectByArtistYearButton.UseVisualStyleBackColor = true;
            this.selectByArtistYearButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // yearGroupBox
            // 
            this.yearGroupBox.Controls.Add(this.selectNoneYearsButton);
            this.yearGroupBox.Controls.Add(this.yearListBox);
            this.yearGroupBox.Controls.Add(this.selectAllYearsButton);
            this.yearGroupBox.Location = new System.Drawing.Point(206, 16);
            this.yearGroupBox.Name = "yearGroupBox";
            this.yearGroupBox.Size = new System.Drawing.Size(110, 204);
            this.yearGroupBox.TabIndex = 1;
            this.yearGroupBox.TabStop = false;
            this.yearGroupBox.Text = "Year";
            // 
            // selectNoneYearsButton
            // 
            this.selectNoneYearsButton.Location = new System.Drawing.Point(53, 175);
            this.selectNoneYearsButton.Name = "selectNoneYearsButton";
            this.selectNoneYearsButton.Size = new System.Drawing.Size(41, 23);
            this.selectNoneYearsButton.TabIndex = 4;
            this.selectNoneYearsButton.Text = "None";
            this.selectNoneYearsButton.UseVisualStyleBackColor = true;
            this.selectNoneYearsButton.Click += new System.EventHandler(this.selectNoneYearsButton_Click);
            // 
            // yearListBox
            // 
            this.yearListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yearListBox.FormattingEnabled = true;
            this.yearListBox.Location = new System.Drawing.Point(6, 19);
            this.yearListBox.Name = "yearListBox";
            this.yearListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.yearListBox.Size = new System.Drawing.Size(98, 147);
            this.yearListBox.TabIndex = 1;
            // 
            // selectAllYearsButton
            // 
            this.selectAllYearsButton.Location = new System.Drawing.Point(6, 175);
            this.selectAllYearsButton.Name = "selectAllYearsButton";
            this.selectAllYearsButton.Size = new System.Drawing.Size(41, 23);
            this.selectAllYearsButton.TabIndex = 3;
            this.selectAllYearsButton.Text = "All";
            this.selectAllYearsButton.UseVisualStyleBackColor = true;
            this.selectAllYearsButton.Click += new System.EventHandler(this.selectAllYearsButton_Click);
            // 
            // artistGroupBox
            // 
            this.artistGroupBox.Controls.Add(this.selectNoneArtistsButton);
            this.artistGroupBox.Controls.Add(this.selectAllArtistsButton);
            this.artistGroupBox.Controls.Add(this.artistListBox);
            this.artistGroupBox.Location = new System.Drawing.Point(6, 16);
            this.artistGroupBox.Name = "artistGroupBox";
            this.artistGroupBox.Size = new System.Drawing.Size(194, 204);
            this.artistGroupBox.TabIndex = 2;
            this.artistGroupBox.TabStop = false;
            this.artistGroupBox.Text = "Artist";
            // 
            // selectNoneArtistsButton
            // 
            this.selectNoneArtistsButton.Location = new System.Drawing.Point(53, 175);
            this.selectNoneArtistsButton.Name = "selectNoneArtistsButton";
            this.selectNoneArtistsButton.Size = new System.Drawing.Size(41, 23);
            this.selectNoneArtistsButton.TabIndex = 2;
            this.selectNoneArtistsButton.Text = "None";
            this.selectNoneArtistsButton.UseVisualStyleBackColor = true;
            this.selectNoneArtistsButton.Click += new System.EventHandler(this.selectNoneArtistsButton_Click);
            // 
            // selectAllArtistsButton
            // 
            this.selectAllArtistsButton.Location = new System.Drawing.Point(6, 175);
            this.selectAllArtistsButton.Name = "selectAllArtistsButton";
            this.selectAllArtistsButton.Size = new System.Drawing.Size(41, 23);
            this.selectAllArtistsButton.TabIndex = 1;
            this.selectAllArtistsButton.Text = "All";
            this.selectAllArtistsButton.UseVisualStyleBackColor = true;
            this.selectAllArtistsButton.Click += new System.EventHandler(this.selectAllArtistsButton_Click);
            // 
            // personGroupBox
            // 
            this.personGroupBox.Controls.Add(this.personListBox);
            this.personGroupBox.Location = new System.Drawing.Point(6, 6);
            this.personGroupBox.Name = "personGroupBox";
            this.personGroupBox.Size = new System.Drawing.Size(310, 214);
            this.personGroupBox.TabIndex = 4;
            this.personGroupBox.TabStop = false;
            this.personGroupBox.Text = "Person";
            // 
            // personListBox
            // 
            this.personListBox.DisplayMember = "FullName";
            this.personListBox.FormattingEnabled = true;
            this.personListBox.Location = new System.Drawing.Point(6, 19);
            this.personListBox.Name = "personListBox";
            this.personListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.personListBox.Size = new System.Drawing.Size(298, 186);
            this.personListBox.TabIndex = 3;
            // 
            // albumFilterModeTabControl
            // 
            this.albumFilterModeTabControl.Controls.Add(this.tabPage1);
            this.albumFilterModeTabControl.Controls.Add(this.tabPage2);
            this.albumFilterModeTabControl.Location = new System.Drawing.Point(6, 19);
            this.albumFilterModeTabControl.Name = "albumFilterModeTabControl";
            this.albumFilterModeTabControl.SelectedIndex = 0;
            this.albumFilterModeTabControl.Size = new System.Drawing.Size(330, 281);
            this.albumFilterModeTabControl.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.artistGroupBox);
            this.tabPage1.Controls.Add(this.selectByArtistYearButton);
            this.tabPage1.Controls.Add(this.yearGroupBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(322, 255);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Artist/Year";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.clearPersonSelectionButton);
            this.tabPage2.Controls.Add(this.selectByPersonButton);
            this.tabPage2.Controls.Add(this.personGroupBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(322, 255);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Person";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // selectByPersonButton
            // 
            this.selectByPersonButton.Location = new System.Drawing.Point(6, 226);
            this.selectByPersonButton.Name = "selectByPersonButton";
            this.selectByPersonButton.Size = new System.Drawing.Size(75, 23);
            this.selectByPersonButton.TabIndex = 5;
            this.selectByPersonButton.Text = "Select";
            this.selectByPersonButton.UseVisualStyleBackColor = true;
            this.selectByPersonButton.Click += new System.EventHandler(this.selectByPersonButton_Click);
            // 
            // clearPersonSelectionButton
            // 
            this.clearPersonSelectionButton.Location = new System.Drawing.Point(87, 226);
            this.clearPersonSelectionButton.Name = "clearPersonSelectionButton";
            this.clearPersonSelectionButton.Size = new System.Drawing.Size(75, 23);
            this.clearPersonSelectionButton.TabIndex = 6;
            this.clearPersonSelectionButton.Text = "Clear";
            this.clearPersonSelectionButton.UseVisualStyleBackColor = true;
            this.clearPersonSelectionButton.Click += new System.EventHandler(this.clearPersonSelectionButton_Click);
            // 
            // BrowseAlbums
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 624);
            this.Controls.Add(this.mainGroupBox);
            this.Name = "BrowseAlbums";
            this.Text = "Browse albums";
            this.mainGroupBox.ResumeLayout(false);
            this.albumInfoGroupBox.ResumeLayout(false);
            this.albumInfoGroupBox.PerformLayout();
            this.albumInfoGeneralInfoGroupBox.ResumeLayout(false);
            this.albumInfoReleasesGroupBox.ResumeLayout(false);
            this.albumPersonnelGroupBox.ResumeLayout(false);
            this.albumPersonnelTabControl.ResumeLayout(false);
            this.musiciansTabPage.ResumeLayout(false);
            this.otherPersonnelTabPage.ResumeLayout(false);
            this.albumInfoYearGroupBox.ResumeLayout(false);
            this.albumInfoYearGroupBox.PerformLayout();
            this.albumInfoTitleGroupBox.ResumeLayout(false);
            this.albumInfoTitleGroupBox.PerformLayout();
            this.albumInfoArtistGroupBox.ResumeLayout(false);
            this.albumInfoArtistGroupBox.PerformLayout();
            this.albumCoverGroupBox.ResumeLayout(false);
            this.albumCoverGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.albumCoverPictureBox)).EndInit();
            this.albumsGroupBox.ResumeLayout(false);
            this.filterGroupBox.ResumeLayout(false);
            this.yearGroupBox.ResumeLayout(false);
            this.artistGroupBox.ResumeLayout(false);
            this.personGroupBox.ResumeLayout(false);
            this.albumFilterModeTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox artistListBox;
        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.GroupBox filterGroupBox;
        private System.Windows.Forms.ListBox yearListBox;
        private System.Windows.Forms.GroupBox albumInfoGroupBox;
        private System.Windows.Forms.GroupBox albumsGroupBox;
        private System.Windows.Forms.ListBox albumsListBox;
        private System.Windows.Forms.GroupBox yearGroupBox;
        private System.Windows.Forms.GroupBox artistGroupBox;
        private System.Windows.Forms.PictureBox albumCoverPictureBox;
        private System.Windows.Forms.Button selectByArtistYearButton;
        private System.Windows.Forms.GroupBox albumCoverGroupBox;
        private System.Windows.Forms.GroupBox albumInfoGeneralInfoGroupBox;
        private System.Windows.Forms.TextBox albumInfoTitleTextBox;
        private System.Windows.Forms.TextBox albumInfoArtistTextBox;
        private System.Windows.Forms.GroupBox albumInfoYearGroupBox;
        private System.Windows.Forms.TextBox albumInfoYearTextBox;
        private System.Windows.Forms.GroupBox albumInfoTitleGroupBox;
        private System.Windows.Forms.GroupBox albumInfoArtistGroupBox;
        private System.Windows.Forms.Button selectNoneYearsButton;
        private System.Windows.Forms.Button selectAllYearsButton;
        private System.Windows.Forms.Button selectNoneArtistsButton;
        private System.Windows.Forms.Button selectAllArtistsButton;
        private System.Windows.Forms.ListBox albumOtherPersonnelListBox;
        private System.Windows.Forms.ListBox albumMusiciansListBox;
        private System.Windows.Forms.GroupBox albumPersonnelGroupBox;
        private System.Windows.Forms.TabControl albumPersonnelTabControl;
        private System.Windows.Forms.TabPage musiciansTabPage;
        private System.Windows.Forms.TabPage otherPersonnelTabPage;
        private System.Windows.Forms.GroupBox albumInfoReleasesGroupBox;
        private System.Windows.Forms.ListBox releasesListBox;
        private System.Windows.Forms.GroupBox personGroupBox;
        private System.Windows.Forms.ListBox personListBox;
        private System.Windows.Forms.TabControl albumFilterModeTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button selectByPersonButton;
        private System.Windows.Forms.Button clearPersonSelectionButton;
    }
}