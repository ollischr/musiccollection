﻿namespace MusicCollectionGUI
{
    partial class NewArtist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rymUrlTextBox = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.facebookUrlTextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.wikipediaUrlTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.homepageUrlTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.addArtistButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.mainGroupBox.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.cancelButton);
            this.mainGroupBox.Controls.Add(this.groupBox6);
            this.mainGroupBox.Controls.Add(this.groupBox7);
            this.mainGroupBox.Controls.Add(this.groupBox5);
            this.mainGroupBox.Controls.Add(this.groupBox3);
            this.mainGroupBox.Controls.Add(this.groupBox4);
            this.mainGroupBox.Controls.Add(this.addArtistButton);
            this.mainGroupBox.Controls.Add(this.groupBox2);
            this.mainGroupBox.Location = new System.Drawing.Point(12, 12);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Size = new System.Drawing.Size(335, 358);
            this.mainGroupBox.TabIndex = 1;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Add artist to DB";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(87, 329);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rymUrlTextBox);
            this.groupBox6.Location = new System.Drawing.Point(6, 227);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(323, 45);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Rateyourmusic URL";
            // 
            // rymUrlTextBox
            // 
            this.rymUrlTextBox.Location = new System.Drawing.Point(6, 19);
            this.rymUrlTextBox.Name = "rymUrlTextBox";
            this.rymUrlTextBox.Size = new System.Drawing.Size(311, 20);
            this.rymUrlTextBox.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.facebookUrlTextBox);
            this.groupBox7.Location = new System.Drawing.Point(6, 125);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(323, 45);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Facebook URL";
            // 
            // facebookUrlTextBox
            // 
            this.facebookUrlTextBox.Location = new System.Drawing.Point(6, 19);
            this.facebookUrlTextBox.Name = "facebookUrlTextBox";
            this.facebookUrlTextBox.Size = new System.Drawing.Size(311, 20);
            this.facebookUrlTextBox.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.wikipediaUrlTextBox);
            this.groupBox5.Location = new System.Drawing.Point(6, 176);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(323, 45);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Wikipedia URL";
            // 
            // wikipediaUrlTextBox
            // 
            this.wikipediaUrlTextBox.Location = new System.Drawing.Point(6, 19);
            this.wikipediaUrlTextBox.Name = "wikipediaUrlTextBox";
            this.wikipediaUrlTextBox.Size = new System.Drawing.Size(311, 20);
            this.wikipediaUrlTextBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.homepageUrlTextBox);
            this.groupBox3.Location = new System.Drawing.Point(6, 74);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(323, 45);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Homepage URL";
            // 
            // homepageUrlTextBox
            // 
            this.homepageUrlTextBox.Location = new System.Drawing.Point(6, 19);
            this.homepageUrlTextBox.Name = "homepageUrlTextBox";
            this.homepageUrlTextBox.Size = new System.Drawing.Size(311, 20);
            this.homepageUrlTextBox.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.urlAliasTextBox);
            this.groupBox4.Location = new System.Drawing.Point(6, 278);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(323, 45);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Location = new System.Drawing.Point(6, 19);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(311, 20);
            this.urlAliasTextBox.TabIndex = 0;
            // 
            // addArtistButton
            // 
            this.addArtistButton.Location = new System.Drawing.Point(6, 329);
            this.addArtistButton.Name = "addArtistButton";
            this.addArtistButton.Size = new System.Drawing.Size(75, 23);
            this.addArtistButton.TabIndex = 6;
            this.addArtistButton.Text = "&Add";
            this.addArtistButton.UseVisualStyleBackColor = true;
            this.addArtistButton.Click += new System.EventHandler(this.addArtistButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nameTextBox);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 45);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Name";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(6, 19);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(311, 20);
            this.nameTextBox.TabIndex = 0;
            this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
            // 
            // NewArtist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 382);
            this.Controls.Add(this.mainGroupBox);
            this.Name = "NewArtist";
            this.Text = "New Artist";
            this.mainGroupBox.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox homepageUrlTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox urlAliasTextBox;
        private System.Windows.Forms.Button addArtistButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox rymUrlTextBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox facebookUrlTextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox wikipediaUrlTextBox;
        private System.Windows.Forms.Button cancelButton;

    }
}