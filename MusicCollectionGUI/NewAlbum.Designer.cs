﻿namespace MusicCollectionGUI
{
    partial class NewAlbum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.artistGroupBox = new System.Windows.Forms.GroupBox();
            this.artistComboBox = new System.Windows.Forms.ComboBox();
            this.personnelGroupBox = new System.Windows.Forms.GroupBox();
            this.personnelListBox = new System.Windows.Forms.ListBox();
            this.resetPersonnelButton = new System.Windows.Forms.Button();
            this.addPersonnelButton = new System.Windows.Forms.Button();
            this.personnelComboBox = new System.Windows.Forms.ComboBox();
            this.MusiciansGroupBox = new System.Windows.Forms.GroupBox();
            this.musiciansListBox = new System.Windows.Forms.ListBox();
            this.resetMusiciansButton = new System.Windows.Forms.Button();
            this.addMusicianButton = new System.Windows.Forms.Button();
            this.musicianComboBox = new System.Windows.Forms.ComboBox();
            this.releaseYearGroupBox = new System.Windows.Forms.GroupBox();
            this.releaseYearTextBox = new System.Windows.Forms.TextBox();
            this.urlaliasGroupBox = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.addAlbumButton = new System.Windows.Forms.Button();
            this.titleGroupBox = new System.Windows.Forms.GroupBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.createNewPersonButton = new System.Windows.Forms.Button();
            this.createNewArtistButton = new System.Windows.Forms.Button();
            this.mainGroupBox.SuspendLayout();
            this.artistGroupBox.SuspendLayout();
            this.personnelGroupBox.SuspendLayout();
            this.MusiciansGroupBox.SuspendLayout();
            this.releaseYearGroupBox.SuspendLayout();
            this.urlaliasGroupBox.SuspendLayout();
            this.titleGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.createNewArtistButton);
            this.mainGroupBox.Controls.Add(this.createNewPersonButton);
            this.mainGroupBox.Controls.Add(this.cancelButton);
            this.mainGroupBox.Controls.Add(this.artistGroupBox);
            this.mainGroupBox.Controls.Add(this.personnelGroupBox);
            this.mainGroupBox.Controls.Add(this.MusiciansGroupBox);
            this.mainGroupBox.Controls.Add(this.releaseYearGroupBox);
            this.mainGroupBox.Controls.Add(this.urlaliasGroupBox);
            this.mainGroupBox.Controls.Add(this.addAlbumButton);
            this.mainGroupBox.Controls.Add(this.titleGroupBox);
            this.mainGroupBox.Location = new System.Drawing.Point(12, 12);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Size = new System.Drawing.Size(335, 461);
            this.mainGroupBox.TabIndex = 2;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Add album to DB";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(87, 432);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // artistGroupBox
            // 
            this.artistGroupBox.Controls.Add(this.artistComboBox);
            this.artistGroupBox.Location = new System.Drawing.Point(6, 19);
            this.artistGroupBox.Name = "artistGroupBox";
            this.artistGroupBox.Size = new System.Drawing.Size(323, 46);
            this.artistGroupBox.TabIndex = 0;
            this.artistGroupBox.TabStop = false;
            this.artistGroupBox.Text = "Artist";
            // 
            // artistComboBox
            // 
            this.artistComboBox.DisplayMember = "Name";
            this.artistComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.artistComboBox.FormattingEnabled = true;
            this.artistComboBox.Location = new System.Drawing.Point(6, 19);
            this.artistComboBox.Name = "artistComboBox";
            this.artistComboBox.Size = new System.Drawing.Size(311, 21);
            this.artistComboBox.TabIndex = 0;
            this.artistComboBox.SelectionChangeCommitted += new System.EventHandler(this.artistComboBox_SelectionChangeCommitted);
            // 
            // personnelGroupBox
            // 
            this.personnelGroupBox.Controls.Add(this.personnelListBox);
            this.personnelGroupBox.Controls.Add(this.resetPersonnelButton);
            this.personnelGroupBox.Controls.Add(this.addPersonnelButton);
            this.personnelGroupBox.Controls.Add(this.personnelComboBox);
            this.personnelGroupBox.Location = new System.Drawing.Point(6, 305);
            this.personnelGroupBox.Name = "personnelGroupBox";
            this.personnelGroupBox.Size = new System.Drawing.Size(323, 121);
            this.personnelGroupBox.TabIndex = 5;
            this.personnelGroupBox.TabStop = false;
            this.personnelGroupBox.Text = "Personnel";
            // 
            // personnelListBox
            // 
            this.personnelListBox.DisplayMember = "FullName";
            this.personnelListBox.FormattingEnabled = true;
            this.personnelListBox.Location = new System.Drawing.Point(6, 46);
            this.personnelListBox.Name = "personnelListBox";
            this.personnelListBox.Size = new System.Drawing.Size(230, 69);
            this.personnelListBox.TabIndex = 1;
            // 
            // resetPersonnelButton
            // 
            this.resetPersonnelButton.Location = new System.Drawing.Point(242, 48);
            this.resetPersonnelButton.Name = "resetPersonnelButton";
            this.resetPersonnelButton.Size = new System.Drawing.Size(75, 23);
            this.resetPersonnelButton.TabIndex = 3;
            this.resetPersonnelButton.Text = "Reset";
            this.resetPersonnelButton.UseVisualStyleBackColor = true;
            this.resetPersonnelButton.Click += new System.EventHandler(this.resetPersonnelButton_Click);
            // 
            // addPersonnelButton
            // 
            this.addPersonnelButton.Location = new System.Drawing.Point(242, 19);
            this.addPersonnelButton.Name = "addPersonnelButton";
            this.addPersonnelButton.Size = new System.Drawing.Size(75, 23);
            this.addPersonnelButton.TabIndex = 2;
            this.addPersonnelButton.Text = "Add";
            this.addPersonnelButton.UseVisualStyleBackColor = true;
            this.addPersonnelButton.Click += new System.EventHandler(this.addPersonnelButton_Click);
            // 
            // personnelComboBox
            // 
            this.personnelComboBox.DisplayMember = "FullName";
            this.personnelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.personnelComboBox.Enabled = false;
            this.personnelComboBox.FormattingEnabled = true;
            this.personnelComboBox.Location = new System.Drawing.Point(6, 19);
            this.personnelComboBox.Name = "personnelComboBox";
            this.personnelComboBox.Size = new System.Drawing.Size(230, 21);
            this.personnelComboBox.TabIndex = 0;
            // 
            // MusiciansGroupBox
            // 
            this.MusiciansGroupBox.Controls.Add(this.musiciansListBox);
            this.MusiciansGroupBox.Controls.Add(this.resetMusiciansButton);
            this.MusiciansGroupBox.Controls.Add(this.addMusicianButton);
            this.MusiciansGroupBox.Controls.Add(this.musicianComboBox);
            this.MusiciansGroupBox.Location = new System.Drawing.Point(6, 173);
            this.MusiciansGroupBox.Name = "MusiciansGroupBox";
            this.MusiciansGroupBox.Size = new System.Drawing.Size(323, 121);
            this.MusiciansGroupBox.TabIndex = 4;
            this.MusiciansGroupBox.TabStop = false;
            this.MusiciansGroupBox.Text = "Musicians";
            // 
            // musiciansListBox
            // 
            this.musiciansListBox.DisplayMember = "FullName";
            this.musiciansListBox.FormattingEnabled = true;
            this.musiciansListBox.Location = new System.Drawing.Point(6, 46);
            this.musiciansListBox.Name = "musiciansListBox";
            this.musiciansListBox.Size = new System.Drawing.Size(230, 69);
            this.musiciansListBox.TabIndex = 1;
            // 
            // resetMusiciansButton
            // 
            this.resetMusiciansButton.Location = new System.Drawing.Point(242, 48);
            this.resetMusiciansButton.Name = "resetMusiciansButton";
            this.resetMusiciansButton.Size = new System.Drawing.Size(75, 23);
            this.resetMusiciansButton.TabIndex = 3;
            this.resetMusiciansButton.Text = "Reset";
            this.resetMusiciansButton.UseVisualStyleBackColor = true;
            this.resetMusiciansButton.Click += new System.EventHandler(this.resetMusiciansButton_Click);
            // 
            // addMusicianButton
            // 
            this.addMusicianButton.Location = new System.Drawing.Point(242, 19);
            this.addMusicianButton.Name = "addMusicianButton";
            this.addMusicianButton.Size = new System.Drawing.Size(75, 23);
            this.addMusicianButton.TabIndex = 2;
            this.addMusicianButton.Text = "Add";
            this.addMusicianButton.UseVisualStyleBackColor = true;
            this.addMusicianButton.Click += new System.EventHandler(this.addMusicianButton_Click);
            // 
            // musicianComboBox
            // 
            this.musicianComboBox.DisplayMember = "FullName";
            this.musicianComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.musicianComboBox.Enabled = false;
            this.musicianComboBox.FormattingEnabled = true;
            this.musicianComboBox.Location = new System.Drawing.Point(6, 19);
            this.musicianComboBox.Name = "musicianComboBox";
            this.musicianComboBox.Size = new System.Drawing.Size(230, 21);
            this.musicianComboBox.TabIndex = 0;
            // 
            // releaseYearGroupBox
            // 
            this.releaseYearGroupBox.Controls.Add(this.releaseYearTextBox);
            this.releaseYearGroupBox.Location = new System.Drawing.Point(234, 71);
            this.releaseYearGroupBox.Name = "releaseYearGroupBox";
            this.releaseYearGroupBox.Size = new System.Drawing.Size(95, 45);
            this.releaseYearGroupBox.TabIndex = 2;
            this.releaseYearGroupBox.TabStop = false;
            this.releaseYearGroupBox.Text = "Release year";
            // 
            // releaseYearTextBox
            // 
            this.releaseYearTextBox.Location = new System.Drawing.Point(6, 19);
            this.releaseYearTextBox.Name = "releaseYearTextBox";
            this.releaseYearTextBox.Size = new System.Drawing.Size(83, 20);
            this.releaseYearTextBox.TabIndex = 2;
            // 
            // urlaliasGroupBox
            // 
            this.urlaliasGroupBox.Controls.Add(this.urlAliasTextBox);
            this.urlaliasGroupBox.Location = new System.Drawing.Point(6, 122);
            this.urlaliasGroupBox.Name = "urlaliasGroupBox";
            this.urlaliasGroupBox.Size = new System.Drawing.Size(323, 45);
            this.urlaliasGroupBox.TabIndex = 3;
            this.urlaliasGroupBox.TabStop = false;
            this.urlaliasGroupBox.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Enabled = false;
            this.urlAliasTextBox.Location = new System.Drawing.Point(6, 19);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(311, 20);
            this.urlAliasTextBox.TabIndex = 3;
            // 
            // addAlbumButton
            // 
            this.addAlbumButton.Location = new System.Drawing.Point(6, 432);
            this.addAlbumButton.Name = "addAlbumButton";
            this.addAlbumButton.Size = new System.Drawing.Size(75, 23);
            this.addAlbumButton.TabIndex = 6;
            this.addAlbumButton.Text = "&Add";
            this.addAlbumButton.UseVisualStyleBackColor = true;
            this.addAlbumButton.Click += new System.EventHandler(this.addAlbumButton_Click);
            // 
            // titleGroupBox
            // 
            this.titleGroupBox.Controls.Add(this.titleTextBox);
            this.titleGroupBox.Location = new System.Drawing.Point(6, 71);
            this.titleGroupBox.Name = "titleGroupBox";
            this.titleGroupBox.Size = new System.Drawing.Size(222, 45);
            this.titleGroupBox.TabIndex = 1;
            this.titleGroupBox.TabStop = false;
            this.titleGroupBox.Text = "Title";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(6, 19);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(210, 20);
            this.titleTextBox.TabIndex = 2;
            this.titleTextBox.TextChanged += new System.EventHandler(this.titleTextBox_TextChanged);
            // 
            // createNewPersonButton
            // 
            this.createNewPersonButton.Location = new System.Drawing.Point(173, 432);
            this.createNewPersonButton.Name = "createNewPersonButton";
            this.createNewPersonButton.Size = new System.Drawing.Size(75, 23);
            this.createNewPersonButton.TabIndex = 4;
            this.createNewPersonButton.Text = "New Person";
            this.createNewPersonButton.UseVisualStyleBackColor = true;
            this.createNewPersonButton.Click += new System.EventHandler(this.createNewPersonButton_Click);
            // 
            // createNewArtistButton
            // 
            this.createNewArtistButton.Location = new System.Drawing.Point(254, 432);
            this.createNewArtistButton.Name = "createNewArtistButton";
            this.createNewArtistButton.Size = new System.Drawing.Size(75, 23);
            this.createNewArtistButton.TabIndex = 8;
            this.createNewArtistButton.Text = "New Artist";
            this.createNewArtistButton.UseVisualStyleBackColor = true;
            this.createNewArtistButton.Click += new System.EventHandler(this.createNewArtistButton_Click);
            // 
            // NewAlbum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 485);
            this.Controls.Add(this.mainGroupBox);
            this.Name = "NewAlbum";
            this.Text = "New Album";
            this.mainGroupBox.ResumeLayout(false);
            this.artistGroupBox.ResumeLayout(false);
            this.personnelGroupBox.ResumeLayout(false);
            this.MusiciansGroupBox.ResumeLayout(false);
            this.releaseYearGroupBox.ResumeLayout(false);
            this.releaseYearGroupBox.PerformLayout();
            this.urlaliasGroupBox.ResumeLayout(false);
            this.urlaliasGroupBox.PerformLayout();
            this.titleGroupBox.ResumeLayout(false);
            this.titleGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.GroupBox personnelGroupBox;
        private System.Windows.Forms.GroupBox MusiciansGroupBox;
        private System.Windows.Forms.GroupBox releaseYearGroupBox;
        private System.Windows.Forms.TextBox releaseYearTextBox;
        private System.Windows.Forms.GroupBox urlaliasGroupBox;
        private System.Windows.Forms.TextBox urlAliasTextBox;
        private System.Windows.Forms.Button addAlbumButton;
        private System.Windows.Forms.GroupBox titleGroupBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.GroupBox artistGroupBox;
        private System.Windows.Forms.ComboBox artistComboBox;
        private System.Windows.Forms.ListBox musiciansListBox;
        private System.Windows.Forms.Button resetMusiciansButton;
        private System.Windows.Forms.Button addMusicianButton;
        private System.Windows.Forms.ComboBox musicianComboBox;
        private System.Windows.Forms.ListBox personnelListBox;
        private System.Windows.Forms.Button resetPersonnelButton;
        private System.Windows.Forms.Button addPersonnelButton;
        private System.Windows.Forms.ComboBox personnelComboBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button createNewArtistButton;
        private System.Windows.Forms.Button createNewPersonButton;
    }
}