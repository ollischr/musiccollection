﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class NewRelease : Form
    {
        private string DBConnectionString { get; set; }

        public NewRelease(string connString)
        {
            InitializeComponent();

            DBConnectionString = connString;

            // populate artist list to combobox
            artistComboBox.DataSource = MusicCollection.MusicCollection.GetArtistsFromMySQLDB(DBConnectionString);

            // populate available media types to combobox
            mediatypeComboBox.Items.AddRange(Enum.GetValues(typeof(MusicCollection.MediaUnitType)).Cast<object>().ToArray());

            // populate record companies to combobox
            recordCompanyComboBox.DataSource = MusicCollection.MusicCollection.GetRecordCompaniesFromMySQLDB(DBConnectionString);

            UpdateComboBoxes();

            Release = new MusicCollection.Release();
        }

        private MusicCollection.MusicCollection LoadedMusicCollection { get; set; }
        private MusicCollection.Release Release { get; set;}

        private void addReleaseButton_Click(object sender, EventArgs e)
        {
            // TODO: check if all required values are present
            Release.Album = (Album)albumComboBox.SelectedItem;
            if (boxsetCheckBox.Checked)
                Release.Boxset = (Boxset)boxsetComboBox.SelectedItem;
            Release.LimitedEdition = limitedEditionCheckBox.Checked;
            Release.Reissue = reissueCheckBox.Checked;
            Release.Remastered = remasterCheckBox.Checked;
            short tempYear;
            if (short.TryParse(releaseYearTextBox.Text, out tempYear))
                Release.Year = tempYear;
            Release.Title = titleTextBox.Text;     // TODO: some value check
            Release.UrlAlias = urlAliasTextBox.Text;    // TODO: some value check

            try
            {
                Release.InsertToMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Trace.WriteLine("Failed to add release to DB");
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Add a media type to the media units list
        /// </summary>
        private void addMediaTypeButton_Click(object sender, EventArgs e)
        {
            Release.MediaUnits.Units.Add((MusicCollection.MediaUnitType)mediatypeComboBox.SelectedItem);
            mediaUnitsTextBox.Text = String.Join("+", Release.MediaUnits.ToString());
        }

        private void boxsetCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (boxsetCheckBox.Checked)
                boxsetComboBox.Enabled = true;
            else
                boxsetComboBox.Enabled = false;
        }

        private void albumComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Album selectedAlbum = (Album)albumComboBox.SelectedItem;
            urlAliasTextBox.Text = Release.GenerateUrlAlias(titleTextBox.Text, selectedAlbum.Title);
            int numOfReleases = selectedAlbum.GetReleasesFromMySQLDB(DBConnectionString).Count();
            releaseYearTextBox.Text = selectedAlbum.Year.ToString();    // fill the release year box with the original album release year
            messagesLabel.Text = "Album has " + numOfReleases.ToString() + " release" + ((numOfReleases == 1) ? "" : "s") + " in DB";
            // TODO: messageLabel should have a link which opens a release browser window
        }

        private void titleTextBox_TextChanged(object sender, EventArgs e)
        {
            Album selectedAlbum = (Album)albumComboBox.SelectedItem;
            urlAliasTextBox.Text = Release.GenerateUrlAlias(titleTextBox.Text, selectedAlbum.Title);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void addRecordCompanyButton_Click(object sender, EventArgs e)
        {
            RecordCompany reccomp = (MusicCollection.RecordCompany)recordCompanyComboBox.SelectedItem;
            if (!Release.RecordCompanies.Contains(reccomp))
                Release.RecordCompanies.Add(reccomp);
            recordCompaniesListBox.DataSource = Release.RecordCompanies.ToArray<RecordCompany>();   // got to convert to array for listbox to update, not sure why?
        }

        private void resetRecordCompaniesButton_Click(object sender, EventArgs e)
        {
            Release.RecordCompanies.Clear();
            recordCompaniesListBox.DataSource = null;
        }

        private void artistComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateComboBoxes();
        }

        /// <summary>
        /// Updates the album and boxset comboboxes according to the selected artist.
        /// </summary>
        private void UpdateComboBoxes()
        {
            Artist selectedArtist = (Artist)artistComboBox.SelectedItem;
            albumComboBox.DataSource = selectedArtist.GetAlbumsFromMySQLDB(DBConnectionString);
            if (albumComboBox.Items.Count > 0)
                albumComboBox.Enabled = true;
            else
                albumComboBox.Enabled = false;

            boxsetComboBox.DataSource = selectedArtist.GetBoxsetsFromMySQLDB(DBConnectionString);
            if (boxsetComboBox.Items.Count > 0)
                boxsetCheckBox.Enabled = true;
            else
                boxsetCheckBox.Enabled = false;
        }

        private void resetMediaTypesButton_Click(object sender, EventArgs e)
        {
            Release.MediaUnits.Units.Clear();
            mediaUnitsTextBox.Text = "";
        }
    }
}
