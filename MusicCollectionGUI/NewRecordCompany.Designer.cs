﻿namespace MusicCollectionGUI
{
    partial class NewRecordCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.facebookUrlGroupBox = new System.Windows.Forms.GroupBox();
            this.facebookUrlTextBox = new System.Windows.Forms.TextBox();
            this.homepageUrlGroupBox = new System.Windows.Forms.GroupBox();
            this.homepageUrlTextBox = new System.Windows.Forms.TextBox();
            this.urlAliasGroupBox = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.addReccompButton = new System.Windows.Forms.Button();
            this.nameGroupBox = new System.Windows.Forms.GroupBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.mainGroupBox.SuspendLayout();
            this.facebookUrlGroupBox.SuspendLayout();
            this.homepageUrlGroupBox.SuspendLayout();
            this.urlAliasGroupBox.SuspendLayout();
            this.nameGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.cancelButton);
            this.mainGroupBox.Controls.Add(this.facebookUrlGroupBox);
            this.mainGroupBox.Controls.Add(this.homepageUrlGroupBox);
            this.mainGroupBox.Controls.Add(this.urlAliasGroupBox);
            this.mainGroupBox.Controls.Add(this.addReccompButton);
            this.mainGroupBox.Controls.Add(this.nameGroupBox);
            this.mainGroupBox.Location = new System.Drawing.Point(12, 12);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Size = new System.Drawing.Size(335, 252);
            this.mainGroupBox.TabIndex = 2;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Add record company to DB";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(87, 223);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // facebookUrlGroupBox
            // 
            this.facebookUrlGroupBox.Controls.Add(this.facebookUrlTextBox);
            this.facebookUrlGroupBox.Location = new System.Drawing.Point(6, 121);
            this.facebookUrlGroupBox.Name = "facebookUrlGroupBox";
            this.facebookUrlGroupBox.Size = new System.Drawing.Size(323, 45);
            this.facebookUrlGroupBox.TabIndex = 2;
            this.facebookUrlGroupBox.TabStop = false;
            this.facebookUrlGroupBox.Text = "Facebook URL";
            // 
            // facebookUrlTextBox
            // 
            this.facebookUrlTextBox.Location = new System.Drawing.Point(6, 19);
            this.facebookUrlTextBox.Name = "facebookUrlTextBox";
            this.facebookUrlTextBox.Size = new System.Drawing.Size(311, 20);
            this.facebookUrlTextBox.TabIndex = 0;
            // 
            // homepageUrlGroupBox
            // 
            this.homepageUrlGroupBox.Controls.Add(this.homepageUrlTextBox);
            this.homepageUrlGroupBox.Location = new System.Drawing.Point(6, 70);
            this.homepageUrlGroupBox.Name = "homepageUrlGroupBox";
            this.homepageUrlGroupBox.Size = new System.Drawing.Size(323, 45);
            this.homepageUrlGroupBox.TabIndex = 1;
            this.homepageUrlGroupBox.TabStop = false;
            this.homepageUrlGroupBox.Text = "Homepage URL";
            // 
            // homepageUrlTextBox
            // 
            this.homepageUrlTextBox.Location = new System.Drawing.Point(6, 19);
            this.homepageUrlTextBox.Name = "homepageUrlTextBox";
            this.homepageUrlTextBox.Size = new System.Drawing.Size(311, 20);
            this.homepageUrlTextBox.TabIndex = 0;
            // 
            // urlAliasGroupBox
            // 
            this.urlAliasGroupBox.Controls.Add(this.urlAliasTextBox);
            this.urlAliasGroupBox.Location = new System.Drawing.Point(6, 172);
            this.urlAliasGroupBox.Name = "urlAliasGroupBox";
            this.urlAliasGroupBox.Size = new System.Drawing.Size(323, 45);
            this.urlAliasGroupBox.TabIndex = 3;
            this.urlAliasGroupBox.TabStop = false;
            this.urlAliasGroupBox.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Location = new System.Drawing.Point(6, 19);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(311, 20);
            this.urlAliasTextBox.TabIndex = 0;
            // 
            // addReccompButton
            // 
            this.addReccompButton.Location = new System.Drawing.Point(6, 223);
            this.addReccompButton.Name = "addReccompButton";
            this.addReccompButton.Size = new System.Drawing.Size(75, 23);
            this.addReccompButton.TabIndex = 4;
            this.addReccompButton.Text = "&Add";
            this.addReccompButton.UseVisualStyleBackColor = true;
            this.addReccompButton.Click += new System.EventHandler(this.addReccompButton_Click);
            // 
            // nameGroupBox
            // 
            this.nameGroupBox.Controls.Add(this.nameTextBox);
            this.nameGroupBox.Location = new System.Drawing.Point(6, 19);
            this.nameGroupBox.Name = "nameGroupBox";
            this.nameGroupBox.Size = new System.Drawing.Size(323, 45);
            this.nameGroupBox.TabIndex = 0;
            this.nameGroupBox.TabStop = false;
            this.nameGroupBox.Text = "Name";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(6, 19);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(311, 20);
            this.nameTextBox.TabIndex = 0;
            this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
            // 
            // NewRecordCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 276);
            this.Controls.Add(this.mainGroupBox);
            this.Name = "NewRecordCompany";
            this.Text = "New Record Company";
            this.mainGroupBox.ResumeLayout(false);
            this.facebookUrlGroupBox.ResumeLayout(false);
            this.facebookUrlGroupBox.PerformLayout();
            this.homepageUrlGroupBox.ResumeLayout(false);
            this.homepageUrlGroupBox.PerformLayout();
            this.urlAliasGroupBox.ResumeLayout(false);
            this.urlAliasGroupBox.PerformLayout();
            this.nameGroupBox.ResumeLayout(false);
            this.nameGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.GroupBox facebookUrlGroupBox;
        private System.Windows.Forms.TextBox facebookUrlTextBox;
        private System.Windows.Forms.GroupBox homepageUrlGroupBox;
        private System.Windows.Forms.TextBox homepageUrlTextBox;
        private System.Windows.Forms.GroupBox urlAliasGroupBox;
        private System.Windows.Forms.TextBox urlAliasTextBox;
        private System.Windows.Forms.Button addReccompButton;
        private System.Windows.Forms.GroupBox nameGroupBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button cancelButton;
    }
}