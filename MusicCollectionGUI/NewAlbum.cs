﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class NewAlbum : Form
    {
        private MusicCollection.Album Album { get; set; }
        private string DBConnectionString { get; set; }
        private int ArtistMusicianCount { get; set; }
        private int ArtistPersonnelCount { get; set; }
        List<Person> FullPeopleList { get; set; }

        public NewAlbum(string connString)
        {
            Album = new Album();

            InitializeComponent();
            DBConnectionString = connString;

            // populate artist list to combobox
            artistComboBox.DataSource = MusicCollection.MusicCollection.GetArtistsFromMySQLDB(DBConnectionString);

            FullPeopleList = MusicCollection.MusicCollection.GetPeopleFromMySQLDB(DBConnectionString);
        }

        private void addMusicianButton_Click(object sender, EventArgs e)
        {
            Person musician = (MusicCollection.Person)musicianComboBox.SelectedItem;
            if (!Album.Musicians.Contains(musician))
                Album.Musicians.Add(musician);
            musiciansListBox.DataSource = Album.Musicians.ToArray<Person>();   // got to convert to array for listbox to update, not sure why?
        }

        private void resetMusiciansButton_Click(object sender, EventArgs e)
        {
            Album.Musicians.Clear();
            musiciansListBox.DataSource = Album.Musicians.ToArray<Person>();
        }

        private void addPersonnelButton_Click(object sender, EventArgs e)
        {
            Person person = (MusicCollection.Person)personnelComboBox.SelectedItem;
            if (!Album.Personnel.Contains(person))
                Album.Personnel.Add(person);
            personnelListBox.DataSource = Album.Personnel.ToArray<Person>();   // got to convert to array for listbox to update, not sure why?
        }

        private void resetPersonnelButton_Click(object sender, EventArgs e)
        {
            Album.Personnel.Clear();
            personnelListBox.DataSource = Album.Personnel.ToArray<Person>();
        }

        private void addAlbumButton_Click(object sender, EventArgs e)
        {
            // TODO: check if all required values are present

            short tempYear;
            if (short.TryParse(releaseYearTextBox.Text, out tempYear))
                Album.Year = tempYear;
            Album.Title = titleTextBox.Text;     // TODO: some value check
            Album.UrlAlias = urlAliasTextBox.Text;    // TODO: some value check
            try
            {
                Album.InsertToMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Trace.WriteLine("Failed to add album to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void titleTextBox_TextChanged(object sender, EventArgs e)
        {
            urlAliasTextBox.Text = Album.GenerateUrlAlias(titleTextBox.Text);   // TODO: check input
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Draw the musician combo box items and make the first x items bold. Stolen from the internet.
        /// </summary>
        private void musicianComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
                return;
            ComboBox combo = ((ComboBox)sender);
            using (SolidBrush brush = new SolidBrush(e.ForeColor))
            {
                Font font = e.Font;
                if (combo.Name == "musicianComboBox" && e.Index < ArtistMusicianCount)  // the artist's musicians are in the beginning of the list
                    font = new System.Drawing.Font(font, FontStyle.Bold);
                if (combo.Name == "personnelComboBox" && e.Index < ArtistPersonnelCount)  // the artist's personnel are in the beginning of the list
                    font = new System.Drawing.Font(font, FontStyle.Bold);
                e.DrawBackground();
                e.Graphics.DrawString(((Person)combo.Items[e.Index]).FullName, font, brush, e.Bounds);
                e.DrawFocusRectangle();
            }
        }

        private void artistComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            RefreshPeopleComboBoxes(false);
        }

        /// <summary>
        /// Load content to musicians and personnel comboboxes from DB.
        /// </summary>
        /// <param name="readFullPeopleList">Re-read the full list of people from the DB. Needs to be done when a new person is added through the "Add person" button.</param>
        private void RefreshPeopleComboBoxes(bool readFullPeopleList)
        {
            // populate musicians and other personnel to comboboxes
            if (artistComboBox.SelectedItem is Artist && Album is Album)
            {
                Album.Artist = (Artist)artistComboBox.SelectedItem;

                // get musicians associated with artist and put them on the top of the list
                if (readFullPeopleList)
                    FullPeopleList = MusicCollection.MusicCollection.GetPeopleFromMySQLDB(DBConnectionString);
                List<Person> artistMusicianList = Album.Artist.GetMusiciansFromMySQLDB(DBConnectionString);
                List<Person> artistPersonnelList = Album.Artist.GetPersonnelFromMySQLDB(DBConnectionString);
                musicianComboBox.DataSource = artistMusicianList.Union(FullPeopleList).ToList();
                personnelComboBox.DataSource = artistPersonnelList.Union(FullPeopleList).ToList();

                // display artist musicians bolded
                // musician count saved to a member variable because passing extra parameters to handlers is a pain in the ass
                ArtistMusicianCount = artistMusicianList.Count;
                ArtistPersonnelCount = artistPersonnelList.Count;
                musicianComboBox.DrawMode = DrawMode.OwnerDrawFixed;
                musicianComboBox.DrawItem += new DrawItemEventHandler(musicianComboBox_DrawItem);

                musicianComboBox.Enabled = true;
                personnelComboBox.Enabled = true;
            }
            else
            {
                musicianComboBox.Enabled = false;
                personnelComboBox.Enabled = false;
            }
        }

        private void createNewPersonButton_Click(object sender, EventArgs e)
        {
            // open new musician form
            NewPerson newPersonForm = new NewPerson(DBConnectionString);
            newPersonForm.ShowDialog();

            // on dialogresult ok refresh combobox and add new musician to listboxes
            if (newPersonForm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                RefreshPeopleComboBoxes(true);
            }
        }

        private void createNewArtistButton_Click(object sender, EventArgs e)
        {
            // open new artist form
            NewArtist newArtistForm = new NewArtist(DBConnectionString);
            newArtistForm.ShowDialog();

            // on dialogresult ok refresh combobox and add new musician to listboxes
            if (newArtistForm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                artistComboBox.DataSource = MusicCollection.MusicCollection.GetArtistsFromMySQLDB(DBConnectionString);
            }
        }
    }
}
