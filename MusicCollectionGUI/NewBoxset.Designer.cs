﻿namespace MusicCollectionGUI
{
    partial class NewBoxset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.releaseYearGroupBox = new System.Windows.Forms.GroupBox();
            this.releaseYearTextBox = new System.Windows.Forms.TextBox();
            this.titleGroupBox = new System.Windows.Forms.GroupBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.artistGroupBox = new System.Windows.Forms.GroupBox();
            this.artistComboBox = new System.Windows.Forms.ComboBox();
            this.urlaliasGroupBox = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.releaseYearGroupBox.SuspendLayout();
            this.titleGroupBox.SuspendLayout();
            this.artistGroupBox.SuspendLayout();
            this.urlaliasGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.urlaliasGroupBox);
            this.groupBox1.Controls.Add(this.cancelButton);
            this.groupBox1.Controls.Add(this.addButton);
            this.groupBox1.Controls.Add(this.releaseYearGroupBox);
            this.groupBox1.Controls.Add(this.titleGroupBox);
            this.groupBox1.Controls.Add(this.artistGroupBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(335, 202);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add boxset to DB";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(87, 173);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(6, 173);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 5;
            this.addButton.Text = "&Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // releaseYearGroupBox
            // 
            this.releaseYearGroupBox.Controls.Add(this.releaseYearTextBox);
            this.releaseYearGroupBox.Location = new System.Drawing.Point(234, 71);
            this.releaseYearGroupBox.Name = "releaseYearGroupBox";
            this.releaseYearGroupBox.Size = new System.Drawing.Size(95, 45);
            this.releaseYearGroupBox.TabIndex = 4;
            this.releaseYearGroupBox.TabStop = false;
            this.releaseYearGroupBox.Text = "Release year";
            // 
            // releaseYearTextBox
            // 
            this.releaseYearTextBox.Location = new System.Drawing.Point(6, 19);
            this.releaseYearTextBox.Name = "releaseYearTextBox";
            this.releaseYearTextBox.Size = new System.Drawing.Size(83, 20);
            this.releaseYearTextBox.TabIndex = 2;
            // 
            // titleGroupBox
            // 
            this.titleGroupBox.Controls.Add(this.titleTextBox);
            this.titleGroupBox.Location = new System.Drawing.Point(6, 71);
            this.titleGroupBox.Name = "titleGroupBox";
            this.titleGroupBox.Size = new System.Drawing.Size(222, 45);
            this.titleGroupBox.TabIndex = 3;
            this.titleGroupBox.TabStop = false;
            this.titleGroupBox.Text = "Title";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(6, 19);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(210, 20);
            this.titleTextBox.TabIndex = 2;
            this.titleTextBox.TextChanged += new System.EventHandler(this.titleTextBox_TextChanged);
            // 
            // artistGroupBox
            // 
            this.artistGroupBox.Controls.Add(this.artistComboBox);
            this.artistGroupBox.Location = new System.Drawing.Point(6, 19);
            this.artistGroupBox.Name = "artistGroupBox";
            this.artistGroupBox.Size = new System.Drawing.Size(323, 46);
            this.artistGroupBox.TabIndex = 1;
            this.artistGroupBox.TabStop = false;
            this.artistGroupBox.Text = "Artist";
            // 
            // artistComboBox
            // 
            this.artistComboBox.DisplayMember = "Name";
            this.artistComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.artistComboBox.FormattingEnabled = true;
            this.artistComboBox.Location = new System.Drawing.Point(6, 19);
            this.artistComboBox.Name = "artistComboBox";
            this.artistComboBox.Size = new System.Drawing.Size(311, 21);
            this.artistComboBox.TabIndex = 0;
            // 
            // urlaliasGroupBox
            // 
            this.urlaliasGroupBox.Controls.Add(this.urlAliasTextBox);
            this.urlaliasGroupBox.Location = new System.Drawing.Point(6, 122);
            this.urlaliasGroupBox.Name = "urlaliasGroupBox";
            this.urlaliasGroupBox.Size = new System.Drawing.Size(323, 45);
            this.urlaliasGroupBox.TabIndex = 7;
            this.urlaliasGroupBox.TabStop = false;
            this.urlaliasGroupBox.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Location = new System.Drawing.Point(6, 19);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(311, 20);
            this.urlAliasTextBox.TabIndex = 3;
            // 
            // NewBoxset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 226);
            this.Controls.Add(this.groupBox1);
            this.Name = "NewBoxset";
            this.Text = "NewBoxset";
            this.groupBox1.ResumeLayout(false);
            this.releaseYearGroupBox.ResumeLayout(false);
            this.releaseYearGroupBox.PerformLayout();
            this.titleGroupBox.ResumeLayout(false);
            this.titleGroupBox.PerformLayout();
            this.artistGroupBox.ResumeLayout(false);
            this.urlaliasGroupBox.ResumeLayout(false);
            this.urlaliasGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox artistGroupBox;
        private System.Windows.Forms.ComboBox artistComboBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.GroupBox releaseYearGroupBox;
        private System.Windows.Forms.TextBox releaseYearTextBox;
        private System.Windows.Forms.GroupBox titleGroupBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.GroupBox urlaliasGroupBox;
        private System.Windows.Forms.TextBox urlAliasTextBox;
    }
}