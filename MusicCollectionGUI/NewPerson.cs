﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class NewPerson : Form
    {
        private string DBConnectionString { get; set; }
        public Person Person { get; set; }

        public NewPerson(string connString)
        {
            InitializeComponent();
            DBConnectionString = connString;
            Person = new Person();
        }

        private void birthDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (birthDateCheckBox.Checked)
                birthDateTimePicker.Enabled = true;
            else
                birthDateTimePicker.Enabled = false;
        }

        private void deathDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (deathDateCheckBox.Checked)
                deathDateTimePicker.Enabled = true;
            else
                deathDateTimePicker.Enabled = false;
        }

        private void firstNameTextBox_TextChanged(object sender, EventArgs e)
        {
            urlAliasTextBox.Text = Person.GenerateUrlAlias(firstNameTextBox.Text, surnameTextBox.Text);
        }

        private void surnameTextBox_TextChanged(object sender, EventArgs e)
        {
            urlAliasTextBox.Text = Person.GenerateUrlAlias(firstNameTextBox.Text, surnameTextBox.Text);
        }

        private void addPersonButton_Click(object sender, EventArgs e)
        {
            Person.Names.Add(new PersonName(firstNameTextBox.Text, surnameTextBox.Text));
            Person.UrlAlias = urlAliasTextBox.Text;
            if (birthDateCheckBox.Checked)
                Person.BirthDate = birthDateTimePicker.Value;
            if (deathDateCheckBox.Checked)
                Person.DeathDate = deathDateTimePicker.Value;
            // TODO: check if person with the same name already exists and ask for user confirmation

            try
            {
                Person.InsertToMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Trace.WriteLine("Failed to add person to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
