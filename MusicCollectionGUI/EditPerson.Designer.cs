﻿namespace MusicCollectionGUI
{
    partial class EditPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.deathDateGroupBox = new System.Windows.Forms.GroupBox();
            this.deathDateCheckBox = new System.Windows.Forms.CheckBox();
            this.deathDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.birthDateGroupBox = new System.Windows.Forms.GroupBox();
            this.birthDateCheckBox = new System.Windows.Forms.CheckBox();
            this.birthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.personGroupBox = new System.Windows.Forms.GroupBox();
            this.personComboBox = new System.Windows.Forms.ComboBox();
            this.okButton = new System.Windows.Forms.Button();
            this.mainGroupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.deathDateGroupBox.SuspendLayout();
            this.birthDateGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.personGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.groupBox4);
            this.mainGroupBox.Controls.Add(this.deathDateGroupBox);
            this.mainGroupBox.Controls.Add(this.birthDateGroupBox);
            this.mainGroupBox.Controls.Add(this.groupBox3);
            this.mainGroupBox.Controls.Add(this.groupBox2);
            this.mainGroupBox.Controls.Add(this.cancelButton);
            this.mainGroupBox.Controls.Add(this.personGroupBox);
            this.mainGroupBox.Controls.Add(this.okButton);
            this.mainGroupBox.Location = new System.Drawing.Point(12, 12);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Size = new System.Drawing.Size(335, 368);
            this.mainGroupBox.TabIndex = 4;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Edit person info";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.urlAliasTextBox);
            this.groupBox4.Location = new System.Drawing.Point(6, 285);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(323, 45);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Enabled = false;
            this.urlAliasTextBox.Location = new System.Drawing.Point(6, 19);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(311, 20);
            this.urlAliasTextBox.TabIndex = 0;
            // 
            // deathDateGroupBox
            // 
            this.deathDateGroupBox.Controls.Add(this.deathDateCheckBox);
            this.deathDateGroupBox.Controls.Add(this.deathDateTimePicker);
            this.deathDateGroupBox.Location = new System.Drawing.Point(6, 234);
            this.deathDateGroupBox.Name = "deathDateGroupBox";
            this.deathDateGroupBox.Size = new System.Drawing.Size(323, 45);
            this.deathDateGroupBox.TabIndex = 11;
            this.deathDateGroupBox.TabStop = false;
            this.deathDateGroupBox.Text = "Death date";
            // 
            // deathDateCheckBox
            // 
            this.deathDateCheckBox.AutoSize = true;
            this.deathDateCheckBox.Location = new System.Drawing.Point(6, 23);
            this.deathDateCheckBox.Name = "deathDateCheckBox";
            this.deathDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.deathDateCheckBox.TabIndex = 0;
            this.deathDateCheckBox.UseVisualStyleBackColor = true;
            // 
            // deathDateTimePicker
            // 
            this.deathDateTimePicker.Enabled = false;
            this.deathDateTimePicker.Location = new System.Drawing.Point(27, 19);
            this.deathDateTimePicker.Name = "deathDateTimePicker";
            this.deathDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.deathDateTimePicker.TabIndex = 1;
            // 
            // birthDateGroupBox
            // 
            this.birthDateGroupBox.Controls.Add(this.birthDateCheckBox);
            this.birthDateGroupBox.Controls.Add(this.birthDateTimePicker);
            this.birthDateGroupBox.Location = new System.Drawing.Point(6, 177);
            this.birthDateGroupBox.Name = "birthDateGroupBox";
            this.birthDateGroupBox.Size = new System.Drawing.Size(323, 45);
            this.birthDateGroupBox.TabIndex = 10;
            this.birthDateGroupBox.TabStop = false;
            this.birthDateGroupBox.Text = "Birth date";
            // 
            // birthDateCheckBox
            // 
            this.birthDateCheckBox.AutoSize = true;
            this.birthDateCheckBox.Location = new System.Drawing.Point(6, 23);
            this.birthDateCheckBox.Name = "birthDateCheckBox";
            this.birthDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.birthDateCheckBox.TabIndex = 0;
            this.birthDateCheckBox.UseVisualStyleBackColor = true;
            // 
            // birthDateTimePicker
            // 
            this.birthDateTimePicker.Enabled = false;
            this.birthDateTimePicker.Location = new System.Drawing.Point(27, 19);
            this.birthDateTimePicker.Name = "birthDateTimePicker";
            this.birthDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.birthDateTimePicker.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.surnameTextBox);
            this.groupBox3.Location = new System.Drawing.Point(6, 126);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(323, 45);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Surname";
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(6, 19);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(311, 20);
            this.surnameTextBox.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.firstNameTextBox);
            this.groupBox2.Location = new System.Drawing.Point(6, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 45);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "First name";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(6, 19);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(311, 20);
            this.firstNameTextBox.TabIndex = 0;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(87, 336);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // personGroupBox
            // 
            this.personGroupBox.Controls.Add(this.personComboBox);
            this.personGroupBox.Location = new System.Drawing.Point(6, 19);
            this.personGroupBox.Name = "personGroupBox";
            this.personGroupBox.Size = new System.Drawing.Size(323, 46);
            this.personGroupBox.TabIndex = 0;
            this.personGroupBox.TabStop = false;
            this.personGroupBox.Text = "Person";
            // 
            // personComboBox
            // 
            this.personComboBox.DisplayMember = "FullName";
            this.personComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.personComboBox.FormattingEnabled = true;
            this.personComboBox.Location = new System.Drawing.Point(6, 19);
            this.personComboBox.Name = "personComboBox";
            this.personComboBox.Size = new System.Drawing.Size(311, 21);
            this.personComboBox.TabIndex = 0;
            this.personComboBox.SelectionChangeCommitted += new System.EventHandler(this.personComboBox_SelectionChangeCommitted);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(6, 336);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // EditPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 390);
            this.Controls.Add(this.mainGroupBox);
            this.Name = "EditPerson";
            this.Text = "Edit Person";
            this.mainGroupBox.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.deathDateGroupBox.ResumeLayout(false);
            this.deathDateGroupBox.PerformLayout();
            this.birthDateGroupBox.ResumeLayout(false);
            this.birthDateGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.personGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox personGroupBox;
        private System.Windows.Forms.ComboBox personComboBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox urlAliasTextBox;
        private System.Windows.Forms.GroupBox deathDateGroupBox;
        private System.Windows.Forms.CheckBox deathDateCheckBox;
        private System.Windows.Forms.DateTimePicker deathDateTimePicker;
        private System.Windows.Forms.GroupBox birthDateGroupBox;
        private System.Windows.Forms.CheckBox birthDateCheckBox;
        private System.Windows.Forms.DateTimePicker birthDateTimePicker;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox firstNameTextBox;
    }
}