﻿namespace MusicCollectionGUI
{
    partial class NewPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.deathDateGroupBox = new System.Windows.Forms.GroupBox();
            this.deathDateCheckBox = new System.Windows.Forms.CheckBox();
            this.deathDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.addPersonButton = new System.Windows.Forms.Button();
            this.birthDateGroupBox = new System.Windows.Forms.GroupBox();
            this.birthDateCheckBox = new System.Windows.Forms.CheckBox();
            this.birthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.mainGroupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.deathDateGroupBox.SuspendLayout();
            this.birthDateGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.cancelButton);
            this.mainGroupBox.Controls.Add(this.groupBox4);
            this.mainGroupBox.Controls.Add(this.deathDateGroupBox);
            this.mainGroupBox.Controls.Add(this.addPersonButton);
            this.mainGroupBox.Controls.Add(this.birthDateGroupBox);
            this.mainGroupBox.Controls.Add(this.groupBox3);
            this.mainGroupBox.Controls.Add(this.groupBox2);
            this.mainGroupBox.Location = new System.Drawing.Point(18, 18);
            this.mainGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mainGroupBox.Size = new System.Drawing.Size(502, 482);
            this.mainGroupBox.TabIndex = 0;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Add person to DB";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(130, 437);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 35);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.urlAliasTextBox);
            this.groupBox4.Location = new System.Drawing.Point(9, 358);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(484, 69);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Location = new System.Drawing.Point(9, 29);
            this.urlAliasTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(464, 26);
            this.urlAliasTextBox.TabIndex = 0;
            // 
            // deathDateGroupBox
            // 
            this.deathDateGroupBox.Controls.Add(this.deathDateCheckBox);
            this.deathDateGroupBox.Controls.Add(this.deathDateTimePicker);
            this.deathDateGroupBox.Location = new System.Drawing.Point(9, 280);
            this.deathDateGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deathDateGroupBox.Name = "deathDateGroupBox";
            this.deathDateGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deathDateGroupBox.Size = new System.Drawing.Size(484, 69);
            this.deathDateGroupBox.TabIndex = 3;
            this.deathDateGroupBox.TabStop = false;
            this.deathDateGroupBox.Text = "Death date";
            // 
            // deathDateCheckBox
            // 
            this.deathDateCheckBox.AutoSize = true;
            this.deathDateCheckBox.Location = new System.Drawing.Point(9, 35);
            this.deathDateCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deathDateCheckBox.Name = "deathDateCheckBox";
            this.deathDateCheckBox.Size = new System.Drawing.Size(22, 21);
            this.deathDateCheckBox.TabIndex = 0;
            this.deathDateCheckBox.UseVisualStyleBackColor = true;
            this.deathDateCheckBox.CheckedChanged += new System.EventHandler(this.deathDateCheckBox_CheckedChanged);
            // 
            // deathDateTimePicker
            // 
            this.deathDateTimePicker.Enabled = false;
            this.deathDateTimePicker.Location = new System.Drawing.Point(40, 29);
            this.deathDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deathDateTimePicker.Name = "deathDateTimePicker";
            this.deathDateTimePicker.Size = new System.Drawing.Size(298, 26);
            this.deathDateTimePicker.TabIndex = 1;
            // 
            // addPersonButton
            // 
            this.addPersonButton.Location = new System.Drawing.Point(9, 437);
            this.addPersonButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addPersonButton.Name = "addPersonButton";
            this.addPersonButton.Size = new System.Drawing.Size(112, 35);
            this.addPersonButton.TabIndex = 5;
            this.addPersonButton.Text = "&Add";
            this.addPersonButton.UseVisualStyleBackColor = true;
            this.addPersonButton.Click += new System.EventHandler(this.addPersonButton_Click);
            // 
            // birthDateGroupBox
            // 
            this.birthDateGroupBox.Controls.Add(this.birthDateCheckBox);
            this.birthDateGroupBox.Controls.Add(this.birthDateTimePicker);
            this.birthDateGroupBox.Location = new System.Drawing.Point(9, 192);
            this.birthDateGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.birthDateGroupBox.Name = "birthDateGroupBox";
            this.birthDateGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.birthDateGroupBox.Size = new System.Drawing.Size(484, 69);
            this.birthDateGroupBox.TabIndex = 2;
            this.birthDateGroupBox.TabStop = false;
            this.birthDateGroupBox.Text = "Birth date";
            // 
            // birthDateCheckBox
            // 
            this.birthDateCheckBox.AutoSize = true;
            this.birthDateCheckBox.Location = new System.Drawing.Point(9, 35);
            this.birthDateCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.birthDateCheckBox.Name = "birthDateCheckBox";
            this.birthDateCheckBox.Size = new System.Drawing.Size(22, 21);
            this.birthDateCheckBox.TabIndex = 0;
            this.birthDateCheckBox.UseVisualStyleBackColor = true;
            this.birthDateCheckBox.CheckedChanged += new System.EventHandler(this.birthDateCheckBox_CheckedChanged);
            // 
            // birthDateTimePicker
            // 
            this.birthDateTimePicker.Enabled = false;
            this.birthDateTimePicker.Location = new System.Drawing.Point(40, 29);
            this.birthDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.birthDateTimePicker.Name = "birthDateTimePicker";
            this.birthDateTimePicker.Size = new System.Drawing.Size(298, 26);
            this.birthDateTimePicker.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.surnameTextBox);
            this.groupBox3.Location = new System.Drawing.Point(9, 114);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(484, 69);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Surname";
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(9, 29);
            this.surnameTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(464, 26);
            this.surnameTextBox.TabIndex = 0;
            this.surnameTextBox.TextChanged += new System.EventHandler(this.surnameTextBox_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.firstNameTextBox);
            this.groupBox2.Location = new System.Drawing.Point(9, 29);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(484, 69);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "First name";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(9, 29);
            this.firstNameTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(464, 26);
            this.firstNameTextBox.TabIndex = 0;
            this.firstNameTextBox.TextChanged += new System.EventHandler(this.firstNameTextBox_TextChanged);
            // 
            // NewPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 518);
            this.Controls.Add(this.mainGroupBox);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "NewPerson";
            this.Text = "New Person";
            this.mainGroupBox.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.deathDateGroupBox.ResumeLayout(false);
            this.deathDateGroupBox.PerformLayout();
            this.birthDateGroupBox.ResumeLayout(false);
            this.birthDateGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.Button addPersonButton;
        private System.Windows.Forms.GroupBox birthDateGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.GroupBox deathDateGroupBox;
        private System.Windows.Forms.DateTimePicker deathDateTimePicker;
        private System.Windows.Forms.DateTimePicker birthDateTimePicker;
        private System.Windows.Forms.CheckBox birthDateCheckBox;
        private System.Windows.Forms.CheckBox deathDateCheckBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox urlAliasTextBox;
        private System.Windows.Forms.Button cancelButton;
    }
}