﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class EditAlbum : Form
    {
        private MusicCollection.Album Album { get; set; }
        private string DBConnectionString { get; set; }
        private int ArtistMusicianCount { get; set; }
        private int ArtistPersonnelCount { get; set; }

        public EditAlbum(string connString)
        {
            InitializeComponent();

            DBConnectionString = connString;

            // populate artist list to combobox
            artistComboBox.DataSource = MusicCollection.MusicCollection.GetArtistsFromMySQLDB(DBConnectionString);

            UpdateAlbumComboBox();
        }

        private void artistComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateAlbumComboBox();
        }

        /// <summary>
        /// Updates the album combobox according to the selected artist.
        /// </summary>
        private void UpdateAlbumComboBox()
        {
            Artist selectedArtist = (Artist)artistComboBox.SelectedItem;
            albumComboBox.DataSource = selectedArtist.GetAlbumsFromMySQLDB(DBConnectionString);
            if (albumComboBox.Items.Count > 0)
                albumComboBox.Enabled = true;
            else
                albumComboBox.Enabled = false;
            UpdateAlbumInfoFields();
        }

        private void albumComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateAlbumInfoFields();
        }

        /// <summary>
        /// Updates the album info fields according to the selected album.
        /// </summary>
        private void UpdateAlbumInfoFields()
        {
            // TODO: update all fields
            if (albumComboBox.SelectedItem is Album) {
                Album = (Album)albumComboBox.SelectedItem;
                Album.UpdateLineupFromMySQLDB(DBConnectionString);
                Album.UpdatePersonnelFromMySQLDB(DBConnectionString);

                titleTextBox.Text = Album.Title;
                releaseYearTextBox.Text = Album.Year.ToString();
                urlAliasTextBox.Text = Album.UrlAlias;
                musiciansListBox.DataSource = Album.Musicians;
                personnelListBox.DataSource = Album.Personnel;

                PopulatePersonComboBoxes();
            }
        }

        private void PopulatePersonComboBoxes()
            {
            // populate musicians and other personnel to comboboxes
            if (artistComboBox.SelectedItem is Artist && Album is Album)
            {
                Album.Artist = (Artist)artistComboBox.SelectedItem;

                // get musicians associated with artist and put them on the top of the list
                List<Person> artistMusicianList = Album.Artist.GetMusiciansFromMySQLDB(DBConnectionString);
                List<Person> artistPersonnelList = Album.Artist.GetPersonnelFromMySQLDB(DBConnectionString);
                List<Person> fullPeopleList = MusicCollection.MusicCollection.GetPeopleFromMySQLDB(DBConnectionString);
                musicianComboBox.DataSource = artistMusicianList.Union(fullPeopleList).ToList();
                personnelComboBox.DataSource = artistPersonnelList.Union(fullPeopleList).ToList();

                // display artist musicians bolded
                // musician count saved to a member variable because passing extra parameters to handlers is a pain in the ass
                ArtistMusicianCount = artistMusicianList.Count;
                ArtistPersonnelCount = artistPersonnelList.Count;
                musicianComboBox.DrawMode = DrawMode.OwnerDrawFixed;
                musicianComboBox.DrawItem += new DrawItemEventHandler(musicianComboBox_DrawItem);

                musicianComboBox.Enabled = true;
                personnelComboBox.Enabled = true;
            }
            else
            {
                musicianComboBox.Enabled = false;
                personnelComboBox.Enabled = false;
            }
        }

        /// <summary>
        /// Draw the musician combo box items and make the first x items bold. Stolen from the internet.
        /// </summary>
        private void musicianComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
                return;
            ComboBox combo = ((ComboBox)sender);
            using (SolidBrush brush = new SolidBrush(e.ForeColor))
            {
                Font font = e.Font;
                if (combo.Name == "musicianComboBox" && e.Index < ArtistMusicianCount)  // the artist's musicians are in the beginning of the list
                    font = new System.Drawing.Font(font, FontStyle.Bold);
                if (combo.Name == "personnelComboBox" && e.Index < ArtistPersonnelCount)  // the artist's personnel are in the beginning of the list
                    font = new System.Drawing.Font(font, FontStyle.Bold);
                e.DrawBackground();
                e.Graphics.DrawString(((Person)combo.Items[e.Index]).FullName, font, brush, e.Bounds);
                e.DrawFocusRectangle();
            }
        }

        private void addMusicianButton_Click(object sender, EventArgs e)
        {
            Person musician = (MusicCollection.Person)musicianComboBox.SelectedItem;
            if (!Album.Musicians.Contains(musician))    // TODO: find out why Contains() returns false when it shouldn't
                Album.Musicians.Add(musician);
            musiciansListBox.DataSource = Album.Musicians.ToArray<Person>();   // got to convert to array for listbox to update, not sure why?
        }

        private void resetMusiciansButton_Click(object sender, EventArgs e)
        {
            Album.Musicians.Clear();
            musiciansListBox.DataSource = Album.Musicians.ToArray<Person>();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            short tempYear;
            if (short.TryParse(releaseYearTextBox.Text, out tempYear))
                Album.Year = tempYear;
            Album.Title = titleTextBox.Text;     // TODO: some value check
            Album.UrlAlias = urlAliasTextBox.Text;    // TODO: some value check
            try
            {
                Album.UpdateMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Console.WriteLine("Failed to add album to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
