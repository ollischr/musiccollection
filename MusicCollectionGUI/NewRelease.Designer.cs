﻿namespace MusicCollectionGUI
{
    partial class NewRelease
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.messagesLabel = new System.Windows.Forms.Label();
            this.recordCompaniesGroupBox = new System.Windows.Forms.GroupBox();
            this.recordCompaniesListBox = new System.Windows.Forms.ListBox();
            this.resetRecordCompaniesButton = new System.Windows.Forms.Button();
            this.addRecordCompanyButton = new System.Windows.Forms.Button();
            this.recordCompanyComboBox = new System.Windows.Forms.ComboBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.boxsetGroupBox = new System.Windows.Forms.GroupBox();
            this.boxsetComboBox = new System.Windows.Forms.ComboBox();
            this.boxsetCheckBox = new System.Windows.Forms.CheckBox();
            this.albumGroupBox = new System.Windows.Forms.GroupBox();
            this.albumComboBox = new System.Windows.Forms.ComboBox();
            this.artistGroupBox = new System.Windows.Forms.GroupBox();
            this.artistComboBox = new System.Windows.Forms.ComboBox();
            this.mediaUnitsGroupBox = new System.Windows.Forms.GroupBox();
            this.mediaUnitsTextBox = new System.Windows.Forms.TextBox();
            this.resetMediaTypesButton = new System.Windows.Forms.Button();
            this.addMediaTypeButton = new System.Windows.Forms.Button();
            this.mediatypeComboBox = new System.Windows.Forms.ComboBox();
            this.urlaliasGroupBox = new System.Windows.Forms.GroupBox();
            this.urlAliasTextBox = new System.Windows.Forms.TextBox();
            this.addTitleGroupBox = new System.Windows.Forms.GroupBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.releaseYearGroupBox = new System.Windows.Forms.GroupBox();
            this.releaseYearTextBox = new System.Windows.Forms.TextBox();
            this.featuresGroupBox = new System.Windows.Forms.GroupBox();
            this.remasterCheckBox = new System.Windows.Forms.CheckBox();
            this.reissueCheckBox = new System.Windows.Forms.CheckBox();
            this.limitedEditionCheckBox = new System.Windows.Forms.CheckBox();
            this.addReleaseButton = new System.Windows.Forms.Button();
            this.mainGroupBox.SuspendLayout();
            this.recordCompaniesGroupBox.SuspendLayout();
            this.boxsetGroupBox.SuspendLayout();
            this.albumGroupBox.SuspendLayout();
            this.artistGroupBox.SuspendLayout();
            this.mediaUnitsGroupBox.SuspendLayout();
            this.urlaliasGroupBox.SuspendLayout();
            this.addTitleGroupBox.SuspendLayout();
            this.releaseYearGroupBox.SuspendLayout();
            this.featuresGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.Controls.Add(this.messagesLabel);
            this.mainGroupBox.Controls.Add(this.recordCompaniesGroupBox);
            this.mainGroupBox.Controls.Add(this.cancelButton);
            this.mainGroupBox.Controls.Add(this.boxsetGroupBox);
            this.mainGroupBox.Controls.Add(this.albumGroupBox);
            this.mainGroupBox.Controls.Add(this.artistGroupBox);
            this.mainGroupBox.Controls.Add(this.mediaUnitsGroupBox);
            this.mainGroupBox.Controls.Add(this.urlaliasGroupBox);
            this.mainGroupBox.Controls.Add(this.addTitleGroupBox);
            this.mainGroupBox.Controls.Add(this.releaseYearGroupBox);
            this.mainGroupBox.Controls.Add(this.featuresGroupBox);
            this.mainGroupBox.Controls.Add(this.addReleaseButton);
            this.mainGroupBox.Location = new System.Drawing.Point(18, 18);
            this.mainGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mainGroupBox.Size = new System.Drawing.Size(502, 772);
            this.mainGroupBox.TabIndex = 3;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Add release to DB";
            // 
            // messagesLabel
            // 
            this.messagesLabel.AutoSize = true;
            this.messagesLabel.Location = new System.Drawing.Point(252, 746);
            this.messagesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.messagesLabel.Name = "messagesLabel";
            this.messagesLabel.Size = new System.Drawing.Size(0, 20);
            this.messagesLabel.TabIndex = 4;
            // 
            // recordCompaniesGroupBox
            // 
            this.recordCompaniesGroupBox.Controls.Add(this.recordCompaniesListBox);
            this.recordCompaniesGroupBox.Controls.Add(this.resetRecordCompaniesButton);
            this.recordCompaniesGroupBox.Controls.Add(this.addRecordCompanyButton);
            this.recordCompaniesGroupBox.Controls.Add(this.recordCompanyComboBox);
            this.recordCompaniesGroupBox.Location = new System.Drawing.Point(9, 368);
            this.recordCompaniesGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.recordCompaniesGroupBox.Name = "recordCompaniesGroupBox";
            this.recordCompaniesGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.recordCompaniesGroupBox.Size = new System.Drawing.Size(333, 146);
            this.recordCompaniesGroupBox.TabIndex = 6;
            this.recordCompaniesGroupBox.TabStop = false;
            this.recordCompaniesGroupBox.Text = "Record companies";
            // 
            // recordCompaniesListBox
            // 
            this.recordCompaniesListBox.DisplayMember = "Name";
            this.recordCompaniesListBox.FormattingEnabled = true;
            this.recordCompaniesListBox.ItemHeight = 20;
            this.recordCompaniesListBox.Location = new System.Drawing.Point(9, 71);
            this.recordCompaniesListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.recordCompaniesListBox.Name = "recordCompaniesListBox";
            this.recordCompaniesListBox.Size = new System.Drawing.Size(235, 64);
            this.recordCompaniesListBox.TabIndex = 3;
            // 
            // resetRecordCompaniesButton
            // 
            this.resetRecordCompaniesButton.Location = new System.Drawing.Point(255, 71);
            this.resetRecordCompaniesButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.resetRecordCompaniesButton.Name = "resetRecordCompaniesButton";
            this.resetRecordCompaniesButton.Size = new System.Drawing.Size(69, 35);
            this.resetRecordCompaniesButton.TabIndex = 2;
            this.resetRecordCompaniesButton.Text = "Reset";
            this.resetRecordCompaniesButton.UseVisualStyleBackColor = true;
            this.resetRecordCompaniesButton.Click += new System.EventHandler(this.resetRecordCompaniesButton_Click);
            // 
            // addRecordCompanyButton
            // 
            this.addRecordCompanyButton.Location = new System.Drawing.Point(255, 26);
            this.addRecordCompanyButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addRecordCompanyButton.Name = "addRecordCompanyButton";
            this.addRecordCompanyButton.Size = new System.Drawing.Size(69, 35);
            this.addRecordCompanyButton.TabIndex = 1;
            this.addRecordCompanyButton.Text = "Add";
            this.addRecordCompanyButton.UseVisualStyleBackColor = true;
            this.addRecordCompanyButton.Click += new System.EventHandler(this.addRecordCompanyButton_Click);
            // 
            // recordCompanyComboBox
            // 
            this.recordCompanyComboBox.DisplayMember = "Name";
            this.recordCompanyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.recordCompanyComboBox.FormattingEnabled = true;
            this.recordCompanyComboBox.Location = new System.Drawing.Point(9, 29);
            this.recordCompanyComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.recordCompanyComboBox.Name = "recordCompanyComboBox";
            this.recordCompanyComboBox.Size = new System.Drawing.Size(235, 28);
            this.recordCompanyComboBox.TabIndex = 0;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(130, 728);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 35);
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // boxsetGroupBox
            // 
            this.boxsetGroupBox.Controls.Add(this.boxsetComboBox);
            this.boxsetGroupBox.Controls.Add(this.boxsetCheckBox);
            this.boxsetGroupBox.Location = new System.Drawing.Point(9, 203);
            this.boxsetGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.boxsetGroupBox.Name = "boxsetGroupBox";
            this.boxsetGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.boxsetGroupBox.Size = new System.Drawing.Size(484, 71);
            this.boxsetGroupBox.TabIndex = 2;
            this.boxsetGroupBox.TabStop = false;
            this.boxsetGroupBox.Text = "Boxset";
            // 
            // boxsetComboBox
            // 
            this.boxsetComboBox.DisplayMember = "Title";
            this.boxsetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.boxsetComboBox.Enabled = false;
            this.boxsetComboBox.FormattingEnabled = true;
            this.boxsetComboBox.Location = new System.Drawing.Point(40, 29);
            this.boxsetComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.boxsetComboBox.Name = "boxsetComboBox";
            this.boxsetComboBox.Size = new System.Drawing.Size(433, 28);
            this.boxsetComboBox.TabIndex = 1;
            // 
            // boxsetCheckBox
            // 
            this.boxsetCheckBox.AutoSize = true;
            this.boxsetCheckBox.Enabled = false;
            this.boxsetCheckBox.Location = new System.Drawing.Point(9, 34);
            this.boxsetCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.boxsetCheckBox.Name = "boxsetCheckBox";
            this.boxsetCheckBox.Size = new System.Drawing.Size(22, 21);
            this.boxsetCheckBox.TabIndex = 0;
            this.boxsetCheckBox.UseVisualStyleBackColor = true;
            this.boxsetCheckBox.CheckedChanged += new System.EventHandler(this.boxsetCheckBox_CheckedChanged);
            // 
            // albumGroupBox
            // 
            this.albumGroupBox.Controls.Add(this.albumComboBox);
            this.albumGroupBox.Location = new System.Drawing.Point(9, 115);
            this.albumGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.albumGroupBox.Name = "albumGroupBox";
            this.albumGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.albumGroupBox.Size = new System.Drawing.Size(484, 71);
            this.albumGroupBox.TabIndex = 1;
            this.albumGroupBox.TabStop = false;
            this.albumGroupBox.Text = "Album";
            // 
            // albumComboBox
            // 
            this.albumComboBox.DisplayMember = "Title";
            this.albumComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.albumComboBox.Enabled = false;
            this.albumComboBox.FormattingEnabled = true;
            this.albumComboBox.Location = new System.Drawing.Point(9, 29);
            this.albumComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.albumComboBox.Name = "albumComboBox";
            this.albumComboBox.Size = new System.Drawing.Size(464, 28);
            this.albumComboBox.TabIndex = 0;
            this.albumComboBox.SelectedIndexChanged += new System.EventHandler(this.albumComboBox_SelectedIndexChanged);
            // 
            // artistGroupBox
            // 
            this.artistGroupBox.Controls.Add(this.artistComboBox);
            this.artistGroupBox.Location = new System.Drawing.Point(9, 29);
            this.artistGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.artistGroupBox.Name = "artistGroupBox";
            this.artistGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.artistGroupBox.Size = new System.Drawing.Size(484, 71);
            this.artistGroupBox.TabIndex = 0;
            this.artistGroupBox.TabStop = false;
            this.artistGroupBox.Text = "Artist";
            // 
            // artistComboBox
            // 
            this.artistComboBox.DisplayMember = "Name";
            this.artistComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.artistComboBox.FormattingEnabled = true;
            this.artistComboBox.Location = new System.Drawing.Point(9, 29);
            this.artistComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.artistComboBox.Name = "artistComboBox";
            this.artistComboBox.Size = new System.Drawing.Size(464, 28);
            this.artistComboBox.TabIndex = 0;
            this.artistComboBox.SelectionChangeCommitted += new System.EventHandler(this.artistComboBox_SelectionChangeCommitted);
            // 
            // mediaUnitsGroupBox
            // 
            this.mediaUnitsGroupBox.Controls.Add(this.mediaUnitsTextBox);
            this.mediaUnitsGroupBox.Controls.Add(this.resetMediaTypesButton);
            this.mediaUnitsGroupBox.Controls.Add(this.addMediaTypeButton);
            this.mediaUnitsGroupBox.Controls.Add(this.mediatypeComboBox);
            this.mediaUnitsGroupBox.Location = new System.Drawing.Point(9, 523);
            this.mediaUnitsGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mediaUnitsGroupBox.Name = "mediaUnitsGroupBox";
            this.mediaUnitsGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mediaUnitsGroupBox.Size = new System.Drawing.Size(234, 117);
            this.mediaUnitsGroupBox.TabIndex = 5;
            this.mediaUnitsGroupBox.TabStop = false;
            this.mediaUnitsGroupBox.Text = "Media units";
            // 
            // mediaUnitsTextBox
            // 
            this.mediaUnitsTextBox.Enabled = false;
            this.mediaUnitsTextBox.Location = new System.Drawing.Point(9, 74);
            this.mediaUnitsTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mediaUnitsTextBox.Name = "mediaUnitsTextBox";
            this.mediaUnitsTextBox.Size = new System.Drawing.Size(136, 26);
            this.mediaUnitsTextBox.TabIndex = 3;
            // 
            // resetMediaTypesButton
            // 
            this.resetMediaTypesButton.Location = new System.Drawing.Point(156, 71);
            this.resetMediaTypesButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.resetMediaTypesButton.Name = "resetMediaTypesButton";
            this.resetMediaTypesButton.Size = new System.Drawing.Size(69, 35);
            this.resetMediaTypesButton.TabIndex = 2;
            this.resetMediaTypesButton.Text = "Reset";
            this.resetMediaTypesButton.UseVisualStyleBackColor = true;
            this.resetMediaTypesButton.Click += new System.EventHandler(this.resetMediaTypesButton_Click);
            // 
            // addMediaTypeButton
            // 
            this.addMediaTypeButton.Location = new System.Drawing.Point(156, 26);
            this.addMediaTypeButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addMediaTypeButton.Name = "addMediaTypeButton";
            this.addMediaTypeButton.Size = new System.Drawing.Size(69, 35);
            this.addMediaTypeButton.TabIndex = 1;
            this.addMediaTypeButton.Text = "Add";
            this.addMediaTypeButton.UseVisualStyleBackColor = true;
            this.addMediaTypeButton.Click += new System.EventHandler(this.addMediaTypeButton_Click);
            // 
            // mediatypeComboBox
            // 
            this.mediatypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mediatypeComboBox.FormattingEnabled = true;
            this.mediatypeComboBox.Location = new System.Drawing.Point(9, 29);
            this.mediatypeComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mediatypeComboBox.Name = "mediatypeComboBox";
            this.mediatypeComboBox.Size = new System.Drawing.Size(136, 28);
            this.mediatypeComboBox.TabIndex = 0;
            // 
            // urlaliasGroupBox
            // 
            this.urlaliasGroupBox.Controls.Add(this.urlAliasTextBox);
            this.urlaliasGroupBox.Location = new System.Drawing.Point(9, 649);
            this.urlaliasGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.urlaliasGroupBox.Name = "urlaliasGroupBox";
            this.urlaliasGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.urlaliasGroupBox.Size = new System.Drawing.Size(484, 69);
            this.urlaliasGroupBox.TabIndex = 7;
            this.urlaliasGroupBox.TabStop = false;
            this.urlaliasGroupBox.Text = "URL alias";
            // 
            // urlAliasTextBox
            // 
            this.urlAliasTextBox.Location = new System.Drawing.Point(9, 29);
            this.urlAliasTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.urlAliasTextBox.Name = "urlAliasTextBox";
            this.urlAliasTextBox.Size = new System.Drawing.Size(464, 26);
            this.urlAliasTextBox.TabIndex = 0;
            // 
            // addTitleGroupBox
            // 
            this.addTitleGroupBox.Controls.Add(this.titleTextBox);
            this.addTitleGroupBox.Location = new System.Drawing.Point(9, 289);
            this.addTitleGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addTitleGroupBox.Name = "addTitleGroupBox";
            this.addTitleGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addTitleGroupBox.Size = new System.Drawing.Size(333, 69);
            this.addTitleGroupBox.TabIndex = 3;
            this.addTitleGroupBox.TabStop = false;
            this.addTitleGroupBox.Text = "Additional title";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(9, 29);
            this.titleTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(313, 26);
            this.titleTextBox.TabIndex = 4;
            this.titleTextBox.TextChanged += new System.EventHandler(this.titleTextBox_TextChanged);
            // 
            // releaseYearGroupBox
            // 
            this.releaseYearGroupBox.Controls.Add(this.releaseYearTextBox);
            this.releaseYearGroupBox.Location = new System.Drawing.Point(351, 289);
            this.releaseYearGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.releaseYearGroupBox.Name = "releaseYearGroupBox";
            this.releaseYearGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.releaseYearGroupBox.Size = new System.Drawing.Size(142, 69);
            this.releaseYearGroupBox.TabIndex = 4;
            this.releaseYearGroupBox.TabStop = false;
            this.releaseYearGroupBox.Text = "Release year";
            // 
            // releaseYearTextBox
            // 
            this.releaseYearTextBox.Location = new System.Drawing.Point(9, 29);
            this.releaseYearTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.releaseYearTextBox.Name = "releaseYearTextBox";
            this.releaseYearTextBox.Size = new System.Drawing.Size(122, 26);
            this.releaseYearTextBox.TabIndex = 5;
            // 
            // featuresGroupBox
            // 
            this.featuresGroupBox.Controls.Add(this.remasterCheckBox);
            this.featuresGroupBox.Controls.Add(this.reissueCheckBox);
            this.featuresGroupBox.Controls.Add(this.limitedEditionCheckBox);
            this.featuresGroupBox.Location = new System.Drawing.Point(351, 368);
            this.featuresGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.featuresGroupBox.Name = "featuresGroupBox";
            this.featuresGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.featuresGroupBox.Size = new System.Drawing.Size(142, 146);
            this.featuresGroupBox.TabIndex = 6;
            this.featuresGroupBox.TabStop = false;
            this.featuresGroupBox.Text = "Features";
            // 
            // remasterCheckBox
            // 
            this.remasterCheckBox.AutoSize = true;
            this.remasterCheckBox.Location = new System.Drawing.Point(9, 100);
            this.remasterCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.remasterCheckBox.Name = "remasterCheckBox";
            this.remasterCheckBox.Size = new System.Drawing.Size(105, 24);
            this.remasterCheckBox.TabIndex = 2;
            this.remasterCheckBox.Text = "Remaster";
            this.remasterCheckBox.UseVisualStyleBackColor = true;
            // 
            // reissueCheckBox
            // 
            this.reissueCheckBox.AutoSize = true;
            this.reissueCheckBox.Location = new System.Drawing.Point(9, 65);
            this.reissueCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.reissueCheckBox.Name = "reissueCheckBox";
            this.reissueCheckBox.Size = new System.Drawing.Size(93, 24);
            this.reissueCheckBox.TabIndex = 1;
            this.reissueCheckBox.Text = "Reissue";
            this.reissueCheckBox.UseVisualStyleBackColor = true;
            // 
            // limitedEditionCheckBox
            // 
            this.limitedEditionCheckBox.AutoSize = true;
            this.limitedEditionCheckBox.Location = new System.Drawing.Point(9, 29);
            this.limitedEditionCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.limitedEditionCheckBox.Name = "limitedEditionCheckBox";
            this.limitedEditionCheckBox.Size = new System.Drawing.Size(86, 24);
            this.limitedEditionCheckBox.TabIndex = 0;
            this.limitedEditionCheckBox.Text = "Limited";
            this.limitedEditionCheckBox.UseVisualStyleBackColor = true;
            // 
            // addReleaseButton
            // 
            this.addReleaseButton.Location = new System.Drawing.Point(9, 728);
            this.addReleaseButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addReleaseButton.Name = "addReleaseButton";
            this.addReleaseButton.Size = new System.Drawing.Size(112, 35);
            this.addReleaseButton.TabIndex = 8;
            this.addReleaseButton.Text = "&Add";
            this.addReleaseButton.UseVisualStyleBackColor = true;
            this.addReleaseButton.Click += new System.EventHandler(this.addReleaseButton_Click);
            // 
            // NewRelease
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 809);
            this.Controls.Add(this.mainGroupBox);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "NewRelease";
            this.Text = "New Release";
            this.mainGroupBox.ResumeLayout(false);
            this.mainGroupBox.PerformLayout();
            this.recordCompaniesGroupBox.ResumeLayout(false);
            this.boxsetGroupBox.ResumeLayout(false);
            this.boxsetGroupBox.PerformLayout();
            this.albumGroupBox.ResumeLayout(false);
            this.artistGroupBox.ResumeLayout(false);
            this.mediaUnitsGroupBox.ResumeLayout(false);
            this.mediaUnitsGroupBox.PerformLayout();
            this.urlaliasGroupBox.ResumeLayout(false);
            this.urlaliasGroupBox.PerformLayout();
            this.addTitleGroupBox.ResumeLayout(false);
            this.addTitleGroupBox.PerformLayout();
            this.releaseYearGroupBox.ResumeLayout(false);
            this.releaseYearGroupBox.PerformLayout();
            this.featuresGroupBox.ResumeLayout(false);
            this.featuresGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox mainGroupBox;
        private System.Windows.Forms.Button addReleaseButton;
        private System.Windows.Forms.GroupBox featuresGroupBox;
        private System.Windows.Forms.CheckBox limitedEditionCheckBox;
        private System.Windows.Forms.CheckBox remasterCheckBox;
        private System.Windows.Forms.CheckBox reissueCheckBox;
        private System.Windows.Forms.GroupBox releaseYearGroupBox;
        private System.Windows.Forms.TextBox releaseYearTextBox;
        private System.Windows.Forms.GroupBox addTitleGroupBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.GroupBox urlaliasGroupBox;
        private System.Windows.Forms.TextBox urlAliasTextBox;
        private System.Windows.Forms.GroupBox mediaUnitsGroupBox;
        private System.Windows.Forms.TextBox mediaUnitsTextBox;
        private System.Windows.Forms.Button resetMediaTypesButton;
        private System.Windows.Forms.Button addMediaTypeButton;
        private System.Windows.Forms.ComboBox mediatypeComboBox;
        private System.Windows.Forms.GroupBox artistGroupBox;
        private System.Windows.Forms.ComboBox artistComboBox;
        private System.Windows.Forms.ComboBox albumComboBox;
        private System.Windows.Forms.ComboBox boxsetComboBox;
        private System.Windows.Forms.CheckBox boxsetCheckBox;
        private System.Windows.Forms.GroupBox boxsetGroupBox;
        private System.Windows.Forms.GroupBox albumGroupBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox recordCompaniesGroupBox;
        private System.Windows.Forms.ListBox recordCompaniesListBox;
        private System.Windows.Forms.Button resetRecordCompaniesButton;
        private System.Windows.Forms.Button addRecordCompanyButton;
        private System.Windows.Forms.ComboBox recordCompanyComboBox;
        private System.Windows.Forms.Label messagesLabel;
    }
}