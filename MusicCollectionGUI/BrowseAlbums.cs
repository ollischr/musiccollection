﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class BrowseAlbums : Form
    {
        private string DBConnectionString { get; set; }
        private string AlbumCoverFilePath { get; set; }
        private List<Album> SelectedAlbums { get; set; }

        public BrowseAlbums(string connString)
        {
            InitializeComponent();
            DBConnectionString = connString;
            SelectedAlbums = new List<Album>();
            AlbumCoverFilePath = MusicCollection.MusicCollection.GetAlbumCoverPath(connString);

            // populate artist listbox
            artistListBox.DataSource = MusicCollection.MusicCollection.GetArtistsFromMySQLDB(DBConnectionString);
            artistListBox.ClearSelected();

            // populate album release year listbox
            yearListBox.DataSource = MusicCollection.MusicCollection.GetAlbumReleaseYearsFromMySQLDB(DBConnectionString);
            yearListBox.ClearSelected();

            // populate person listbox
            personListBox.DataSource = MusicCollection.MusicCollection.GetPeopleFromMySQLDB(DBConnectionString);
            personListBox.ClearSelected();
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            SelectedAlbums = MusicCollection.MusicCollection.GetAlbumsFromMySQLDB(DBConnectionString, artistListBox.SelectedItems.Cast<Artist>().ToList(), yearListBox.SelectedItems.Cast<int>().ToList());
            albumsListBox.DataSource = SelectedAlbums;
        }

        private void albumsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateAlbumInfoBox((Album)albumsListBox.SelectedItem);
        }

        private void UpdateAlbumInfoBox(Album album)
        {
            string path = Path.Combine(AlbumCoverFilePath, album.ID.ToString().PadLeft(4, '0') + ".jpg");  // TODO: support for other formats

            // update artist info from DB
            album.Artist.ReadInfoFromMySQLDB(DBConnectionString);
            album.UpdateLineupFromMySQLDB(DBConnectionString);
            album.UpdatePersonnelFromMySQLDB(DBConnectionString);

            // draw album cover
            // TODO: resize album cover to 100px width, preserve aspect ratio
            if (File.Exists(path))
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                    albumCoverPictureBox.Image = Image.FromStream(stream);
            else
                albumCoverPictureBox.Image = null;

            // update album info boxes
            albumInfoTitleTextBox.Text = album.Title;
            albumInfoYearTextBox.Text = album.Year.ToString();
            albumInfoArtistTextBox.Text = album.Artist.Name;

            albumMusiciansListBox.DataSource = album.Musicians;
            albumOtherPersonnelListBox.DataSource = album.Personnel;
        
            // update releases
            List<Release> releases = album.GetReleasesFromMySQLDB(DBConnectionString);
            releasesListBox.DataSource = releases;
        }

        /// <summary>
        /// Select all artists.
        /// </summary>
        private void selectAllArtistsButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < artistListBox.Items.Count; i++)
                artistListBox.SetSelected(i, true);
        }

        /// <summary>
        /// Unselect all artists.
        /// </summary>
        private void selectNoneArtistsButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < artistListBox.Items.Count; i++)
                artistListBox.SetSelected(i, false);
        }

        /// <summary>
        /// Select all release years.
        /// </summary>
        private void selectAllYearsButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < yearListBox.Items.Count; i++)
                yearListBox.SetSelected(i, true);
        }

        /// <summary>
        /// Unselect all release years.
        /// </summary>
        private void selectNoneYearsButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < yearListBox.Items.Count; i++)
                yearListBox.SetSelected(i, false);
        }

        private void selectByPersonButton_Click(object sender, EventArgs e)
        {
            SelectedAlbums = MusicCollection.MusicCollection.GetAlbumsFromMySQLDB(DBConnectionString, personListBox.SelectedItems.Cast<Person>().ToList(), true, false);
            albumsListBox.DataSource = SelectedAlbums;
        }

        private void clearPersonSelectionButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < personListBox.Items.Count; i++)
                personListBox.SetSelected(i, false);
        }
    }
}
