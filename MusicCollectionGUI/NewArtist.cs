﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class NewArtist : Form
    {
        private string DBConnectionString { get; set; }

        public NewArtist(string connString)
        {
            InitializeComponent();

            DBConnectionString = connString;
        }

        private void addArtistButton_Click(object sender, EventArgs e)
        {
            Artist artist = new Artist();
            artist.Name = nameTextBox.Text;
            artist.UrlAlias = urlAliasTextBox.Text;
            if (Uri.IsWellFormedUriString(homepageUrlTextBox.Text, UriKind.Absolute))
                artist.UrlHome = new Uri(homepageUrlTextBox.Text);
            if (Uri.IsWellFormedUriString(rymUrlTextBox.Text, UriKind.Absolute))
                artist.UrlRateYourMusic = new Uri(rymUrlTextBox.Text);
            if (Uri.IsWellFormedUriString(facebookUrlTextBox.Text, UriKind.Absolute))
                artist.UrlFacebook = new Uri(facebookUrlTextBox.Text);
            if (Uri.IsWellFormedUriString(wikipediaUrlTextBox.Text, UriKind.Absolute))
                artist.UrlWikipedia = new Uri(wikipediaUrlTextBox.Text);
            try
            {
                artist.InsertToMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Trace.WriteLine("Failed to add artist to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            urlAliasTextBox.Text = MusicCollection.Artist.GenerateUrlAlias(nameTextBox.Text);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
