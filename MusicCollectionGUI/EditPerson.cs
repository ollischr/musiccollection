﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class EditPerson : Form
    {
        private MusicCollection.Person Person { get; set; }
        private string DBConnectionString { get; set; }

        public EditPerson(string connString)
        {
            InitializeComponent();

            DBConnectionString = connString;

            // populate people list to combobox
            personComboBox.DataSource = MusicCollection.MusicCollection.GetPeopleFromMySQLDB(DBConnectionString);

            UpdatePersonInfoFields();
        }

        /// <summary>
        /// Updates the person info fields according to the selected person.
        /// </summary>
        private void UpdatePersonInfoFields()
        {
            // update all fields
            if (personComboBox.SelectedItem is Person)
            {
                Person = (Person)personComboBox.SelectedItem;

                // TODO: support for multiple names
                firstNameTextBox.Text = Person.Names.First().FirstName;
                surnameTextBox.Text = Person.Names.First().Surname;
                urlAliasTextBox.Text = Person.UrlAlias;
                // TODO: possibility to have null birthdates and deathdates...
                if (Person.BirthDate >= birthDateTimePicker.MinDate && Person.BirthDate <= birthDateTimePicker.MaxDate)
                {
                    birthDateTimePicker.Value = Person.BirthDate;
                    birthDateCheckBox.Checked = true;
                }
                else
                    birthDateCheckBox.Checked = false;

                if (Person.DeathDate >= deathDateTimePicker.MinDate && Person.DeathDate <= deathDateTimePicker.MaxDate)
                {
                    deathDateTimePicker.Value = Person.DeathDate;
                    deathDateCheckBox.Checked = true;
                }
                else
                    deathDateCheckBox.Checked = false;
            }
        }

        private void personComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdatePersonInfoFields();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            // TODO: support for multiple names
            Person.Names.Clear();
            Person.Names.Add(new PersonName(firstNameTextBox.Text, surnameTextBox.Text));
            if (birthDateCheckBox.Checked)
                Person.BirthDate = birthDateTimePicker.Value;
            if (deathDateCheckBox.Checked)
                Person.DeathDate = deathDateTimePicker.Value;
            Person.UrlAlias = urlAliasTextBox.Text;

            try
            {
                Person.UpdateMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Console.WriteLine("Failed to add album to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
