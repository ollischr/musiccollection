﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class NewBoxset : Form
    {
        private MusicCollection.Boxset Boxset { get; set; }
        private string DBConnectionString { get; set; }

        public NewBoxset(string connString)
        {
            InitializeComponent();
            DBConnectionString = connString;

            // populate artist list to combobox
            artistComboBox.DataSource = MusicCollection.MusicCollection.GetArtistsFromMySQLDB(DBConnectionString);

            Boxset = new Boxset();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            // TODO: check if all required values are present
            Boxset.Artist = (Artist)artistComboBox.SelectedItem;
            Boxset.Title = titleTextBox.Text;
            short tempYear;
            if (short.TryParse(releaseYearTextBox.Text, out tempYear))
                Boxset.Year = tempYear;
            Boxset.UrlAlias = urlAliasTextBox.Text;    // TODO: some value check

            try
            {
                Boxset.InsertToMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Trace.WriteLine("Failed to add boxset to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void titleTextBox_TextChanged(object sender, EventArgs e)
        {
            urlAliasTextBox.Text = Boxset.GenerateUrlAlias(titleTextBox.Text);
        }
    }
}
