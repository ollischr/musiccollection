﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicCollection;

namespace MusicCollectionGUI
{
    public partial class NewRecordCompany : Form
    {
        private string DBConnectionString { get; set; }

        public NewRecordCompany(string connString)
        {
            InitializeComponent();

            DBConnectionString = connString;
        }

        /// <summary>
        /// The text in the Name textbox changes, update the urlalias field in real time.
        /// </summary>
        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            urlAliasTextBox.Text = MusicCollection.RecordCompany.GenerateUrlAlias(nameTextBox.Text);
        }

        /// <summary>
        /// User has pressed Add. Create new RecordCompany object ja add its info to the DB.
        /// </summary>
        private void addReccompButton_Click(object sender, EventArgs e)
        {
            RecordCompany reccomp = new RecordCompany();
            reccomp.Name = nameTextBox.Text;        // TODO: check value
            reccomp.UrlAlias = urlAliasTextBox.Text;    // TODO: check value
            if (Uri.IsWellFormedUriString(homepageUrlTextBox.Text, UriKind.Absolute))
                reccomp.UrlHome = new Uri(homepageUrlTextBox.Text);
            if (Uri.IsWellFormedUriString(facebookUrlTextBox.Text, UriKind.Absolute))
                reccomp.UrlFacebook = new Uri(facebookUrlTextBox.Text);

            try
            {
                reccomp.InsertToMySQLDB(DBConnectionString);
            }
            catch (MusicCollectionException)
            {
                Trace.WriteLine("Failed to add record company to DB");
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        /// <summary>
        /// User has pressed cancel, just exit.
        /// </summary>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
