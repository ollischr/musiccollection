﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace MusicCollection
{
    public class Person : MusicEntity
    {
        private List<PersonName> _names = new List<PersonName>();

        // TODO: input value checks to setters
        public string FullName { get { return (Names.First().FirstName + " " + Names.First().Surname).Trim(); } }
        public List<PersonName> Names { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime DeathDate { get; set; }
        public override string UrlAlias
        {
            get {
                if (_urlalias.Length == 0)
                    UrlAlias = GenerateUrlAlias();
                return _urlalias;
            }
            set { _urlalias = value; }
        }

        public Person(int id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Person(string id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Person() : base() { }
        public Person(MySqlDataReader reader)
            : base()
        {
            ReadInfoFromMySqlDataReader(reader);
        }

        /// <summary>
        /// Setup stuff to be done when an instance is created.
        /// </summary>
        protected override void Setup()
        {
            DBTableName = MusicDBTableNames.Persons;
            Names = new List<PersonName>();
        }

        public static string GenerateUrlAlias(string firstname, string surname)
        {
            // TODO: replace umlauts and other weird characters with "normal" ones, e.g. áöü => aou
            firstname = firstname.ToLower();
            surname = surname.ToLower();
            return Regex.Replace(firstname, @"[^A-Za-z0-9_\.~]+", "_") + "_" + Regex.Replace(surname, @"[^A-Za-z0-9_\.~]+", "_");
        }

        protected override string GenerateUrlAlias() {
            return GenerateUrlAlias(Names.First().FirstName, Names.First().Surname);
        }

        public override void ReadInfoFromMySQLDB(string dbConnString)
        {
            // read person info from MySQL DB
            string cmdString = @"SELECT per.id, per.firstname, per.surname, per.birthdate, per.deathdate, per.urlalias
                FROM " + MusicDBTableNames.Persons + " per " +
                "WHERE per.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, ReadInfoFromMySqlDataReader);
        }

        /// <summary>
        /// Updates person data from a MySqlDataReader object. The DataReader needs to be pointing to a data row. This method does not advance the reader, just gets the info
        /// from the current row.
        /// </summary>
        /// <param name="reader">MySqlDataReader object to read.</param>
        protected override void ReadInfoFromMySqlDataReader(MySqlDataReader reader)
        {
            SetID(reader["id"].ToString());
            Names.Add(new PersonName(reader["firstname"].ToString(), reader["surname"].ToString()));
            DateTime birthDate, deathDate;
            if (DateTime.TryParse(reader["birthdate"].ToString(), out birthDate))
                BirthDate = birthDate;
            if (DateTime.TryParse(reader["deathdate"].ToString(), out deathDate))
                DeathDate = deathDate;
            UrlAlias = reader["urlalias"].ToString();
        }

        public override void InsertToMySQLDB(string dbConnString)
        {
            // check if urlalias is available before inserting
            if (!CheckUrlAliasAvailability(UrlAlias, dbConnString))
                throw new UrlAliasNotAvailableException("person, urlalias: " + UrlAlias);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.Persons + "(firstname,surname,birthdate,deathdate,urlalias) " +
                "VALUES (@firstname,@surname,@birthdate,@deathdate,@urlalias)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@firstname", Names.First().FirstName);
                    cmd.Parameters.AddWithValue("@surname", Names.First().Surname);
                    cmd.Parameters.AddWithValue("@birthdate", (BirthDate == DateTime.MinValue) ? null : BirthDate.ToString());
                    cmd.Parameters.AddWithValue("@deathdate", (DeathDate == DateTime.MinValue) ? null : DeathDate.ToString());
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when adding person to DB");
                    ID = (int)cmd.LastInsertedId;
                    Console.WriteLine("Inserted person to DB, ID: " + ID.ToString());
                }
                dbConnection.Close();
            }
        }

        public override void UpdateMySQLDB(string dbConnString)
        {
            string cmdString = @"UPDATE " + MusicDBTableNames.Persons + " SET " +
                "firstname = @firstname, " +
                "surname = @surname, " +
                "birthdate = @birthdate, " +
                "deathdate = @deathdate " +
                "urlalias = @urlalias" +
                "WHERE id = @id";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@firstname", Names.First().FirstName);
                    cmd.Parameters.AddWithValue("@surname", Names.First().Surname);
                    cmd.Parameters.AddWithValue("@birthdate", (BirthDate == DateTime.MinValue) ? null : BirthDate.ToString());
                    cmd.Parameters.AddWithValue("@deathdate", (DeathDate == DateTime.MinValue) ? null : DeathDate.ToString());
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Parameters.AddWithValue("@id", ID);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when updating album in DB");
                }
                dbConnection.Close();
            }
        }

        /// <summary>
        /// Delete person from DB.
        /// </summary>
        public override void DeleteFromMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }
    }
}
