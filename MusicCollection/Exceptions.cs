﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace MusicCollection
{
    public class MusicCollectionException : Exception
    {
        public MusicCollectionException()
        {
        }

        public MusicCollectionException(string message)
            : base(message)
        {
        }

        public MusicCollectionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class UrlAliasNotAvailableException : MusicCollectionException
    {
        public UrlAliasNotAvailableException()
        {
        }

        public UrlAliasNotAvailableException(string message)
            : base(message)
        {
        }

        public UrlAliasNotAvailableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class MusicCollectionDBException : MusicCollectionException
    {
        public MusicCollectionDBException() { }
        public MusicCollectionDBException(string message)
            : base(message)
        {
        }

        public MusicCollectionDBException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}