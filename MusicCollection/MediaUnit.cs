﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicCollection
{
    public class MediaUnitSet
    {
        public List<MediaUnitType> Units { get; protected set; }

        public MediaUnitSet()
        {
            Units = new List<MediaUnitType>();
        }

        /// <summary>
        /// Generates a nice looking string from the list of media unit types (e.g. "2CD+DVD")
        /// </summary>
        /// <returns>A nice looking string</returns>
        public override string ToString()
        {
            string result = "";

            foreach (MediaUnitType jeps in Units.Distinct())
            {
                int typeCount = Units.Count(x => x == jeps);
                result += ((typeCount == 1) ? "" : typeCount.ToString()) + jeps.ToString() + "+";

            }
            result = result.TrimEnd('+');

            return result;
        }
    }
}
