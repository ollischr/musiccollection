﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicCollection
{
    public class PersonName
    {
        // TODO: input value checks to setters
        public string FirstName { get; protected set; }
        public string Surname { get; protected set; }

        public PersonName(string firstName, string surname)
        {
            FirstName = firstName;
            Surname = surname;
        }
    }
}
