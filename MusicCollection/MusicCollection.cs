﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollection
{
    public class MusicCollection
    {
        public List<Artist> Artists {get; set;}

        public MusicCollection()
        {
        }

        /// <summary>
        /// Get the file system path for the album cover images from the DB.
        /// </summary>
        /// <param name="dbConnString">Databse connection string.</param>
        /// <returns>Album cover image directory as string.</returns>
        // TODO: verify that string is a valid path, maybe even check that the path exists on this machine?
        // TODO: throw exception if record cannot be found in DB?
        public static string GetAlbumCoverPath(string dbConnString)
        {
            string pathName = "";
            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of artists from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM " + MusicDBTableNames.FilePaths + " WHERE name = 'album_covers'", DBConnection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            pathName = reader["path"].ToString();
                    }
                }
                DBConnection.Close();
            }
            return pathName;
        }

        public static List<Artist> GetArtistsFromMySQLDB(string dbConnString)
        {
            List<Artist> artists = new List<Artist>();
            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of artists from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM " + MusicDBTableNames.Artists + " ORDER BY name", DBConnection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            artists.Add(new Artist(reader));
                    }
                }
                DBConnection.Close();
            }
            return artists;
        }

        public static List<Person> GetPeopleFromMySQLDB(string dbConnString)
        {
            List<Person> people = new List<Person>();
            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of releases from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM persons ORDER BY surname", DBConnection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                        while (reader.Read())
                            people.Add(new Person(reader));
                }
                DBConnection.Close();
            }
            return people;
        }

        public static List<RecordCompany> GetRecordCompaniesFromMySQLDB(string dbConnString)
        {
            List<RecordCompany> reccomps = new List<RecordCompany>();
            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of record companies from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM " + MusicDBTableNames.RecordCompanies + " ORDER BY name", DBConnection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                        while (reader.Read())
                            reccomps.Add(new RecordCompany(reader));
                }
                DBConnection.Close();
            }
            return reccomps;
        }

        /// <summary>
        /// Get list of all album release years currently in the DB.
        /// </summary>
        /// <param name="dbConnString">MySQL DB connection string.</param>
        /// <returns>List of release years.</returns>
        public static List<int> GetAlbumReleaseYearsFromMySQLDB(string dbConnString)
        {
            List<int> years = new List<int>();
            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of album release years from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand("select distinct year_original from " + MusicDBTableNames.Albums + " order by year_original", DBConnection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                        while (reader.Read())
                            years.Add(Int16.Parse(reader["year_original"].ToString()));
                }
                DBConnection.Close();
            }
            return years;
        }

        /// <summary>
        /// Get albums that fill the search criteria.
        /// </summary>
        /// <param name="dbConnString">MySQL DB connection string.</param>
        /// <param name="artists">List of artists.</param>
        /// <param name="years">List of years.</param>
        /// <returns>List of albums.</returns>
        // TODO: add other search parameters: musicians, record companies etc.
        public static List<Album> GetAlbumsFromMySQLDB(string dbConnString, List<Artist> artists, List<int> years)
        {
            List<Album> albums = new List<Album>();

            // construct MySQL query, implementation kinda sucks but works
            string query = "select * from " + MusicDBTableNames.Albums + " alb";
            
            if (artists.Count > 0)
            {
                query += " WHERE (";
                for (int i = 0; i < artists.Count; i++) { query += "id_artist = @id_artist" + i + " OR "; }
                query = query.Substring(0, query.Length - 4) + ")"; // remove the trailing " OR "
            }
            if (years.Count > 0)
            {
                if (artists.Count > 0)
                    query += " AND (";
                else
                    query += " WHERE (";
                for (int i = 0; i < years.Count; i++) { query += "year_original = @year_original" + i + " OR "; }
                query = query.Substring(0, query.Length - 4) + ")"; // remove the trailing " OR "
            }
            query += " ORDER BY year_original";

            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of albums from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand(query, DBConnection))
                {
                    for (int i = 0; i < artists.Count; i++) { cmd.Parameters.AddWithValue("id_artist" + i, artists[i].ID); }
                    for (int i = 0; i < years.Count; i++) { cmd.Parameters.AddWithValue("year_original" + i, years[i]); }
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                        while (reader.Read())
                            albums.Add(new Album(reader));
                }
                DBConnection.Close();
            }
            return albums;
        }

        /// <summary>
        /// Get albums by specific artists.
        /// </summary>
        /// <param name="dbConnString">MySQL DB connection string</param>
        /// <param name="artists">List of artists.</param>
        /// <returns>List of albums.</returns>
        public static List<Album> GetAlbumsFromMySQLDB(string dbConnString, List<Artist> artists)
        {
            return GetAlbumsFromMySQLDB(dbConnString, artists, new List<int> { });
        }

        /// <summary>
        /// Get albums by specific release years.
        /// </summary>
        /// <param name="dbConnString">MySQL DB connection string</param>
        /// <param name="years">List of release years.</param>
        /// <returns>List of albums.</returns>
        public static List<Album> GetAlbumsFromMySQLDB(string dbConnString, List<int> years)
        {
            return GetAlbumsFromMySQLDB(dbConnString, new List<Artist> { }, years);
        }

        // TODO: Method for getting albums by specific musicians, SQL search something like below:
        //SELECT alb.*
        //FROM albums alb
        //    INNER JOIN albums_and_musicians aam
        //        ON alb.id = aam.id_album
        //    INNER JOIN persons per
        //        ON aam.id_person = per.id
        //WHERE per.id in (241, 549)
        //GROUP BY alb.id
        //HAVING COUNT(DISTINCT per.id) = 2;
        public static List<Album> GetAlbumsFromMySQLDB(string dbConnString, List<Person> musicians, bool searchMusicians, bool searchPersonnel)
        {
            // TODO: search personnel table as well
            
            List<Album> albums = new List<Album>();

            // return empty album list if musician list is empty
            if (musicians.Count == 0)
                return albums;

            string query = "select * from " + MusicDBTableNames.Albums + " alb";
            if (searchMusicians)
            {
                query += " INNER JOIN " + MusicDBTableNames.AlbumsAndMusicians + " aam on alb.id = aam.id_album";
                query += " INNER JOIN " + MusicDBTableNames.Persons + " per on aam.id_person = per.id";
                query += " WHERE per.id in (" + String.Join(",", musicians.Select(x => x.ID).ToArray()) + ")";
                query += " GROUP BY alb.id HAVING COUNT(DISTINCT per.id) = " + musicians.Count.ToString();
            }

            query += " ORDER BY year_original";

            using (MySqlConnection DBConnection = new MySqlConnection(dbConnString))
            {
                DBConnection.Open();
                // read list of albums from MySQL DB
                using (MySqlCommand cmd = new MySqlCommand(query, DBConnection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                        while (reader.Read())
                            albums.Add(new Album(reader));
                }
                DBConnection.Close();
            }
            return albums;
        }
    }
}
