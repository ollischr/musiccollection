﻿using System.Text;
using System.Threading.Tasks;

namespace MusicCollection
{
    public enum MediaUnitType { CD, LP, MC, DVD, BD }

    public static class MusicDBTableNames
    {
        public const string Releases = "releases";
        public const string Albums = "albums";
        public const string Persons = "persons";
        public const string Artists = "artists";
        public const string RecordCompanies = "recordcompanies";
        public const string Boxsets = "boxsets";
        public const string ReleasesAndRecordCompanies = "releases_and_recordcompanies";
        public const string AlbumsAndMusicians = "albums_and_musicians";
        public const string AlbumsAndPersonnel = "albums_and_personnel";
        public const string FilePaths = "file_paths";
    }
}