﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollection
{
    public class RecordCompany : MusicEntity
    {
        // TODO: input value checks to setters
        public string Name { get; set; }
        public Uri UrlHome { get; set; }
        public Uri UrlFacebook { get; set; }
        protected List<Release> Releases { get; set; }

        public RecordCompany(int id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public RecordCompany(string id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public RecordCompany() : base() { }
        public RecordCompany(MySqlDataReader reader)
            : base()
        {
            ReadInfoFromMySqlDataReader(reader);
        }

        protected override void Setup()
        {
            DBTableName = MusicDBTableNames.RecordCompanies;
            Releases = new List<Release>();
        }

        public override void ReadInfoFromMySQLDB(string dbConnString)
        {
            // read record company info from MySQL DB
            string cmdString = @"SELECT rec.id, rec.name, rec.url_home, rec.url_facebook, rec.urlalias " +
                "FROM " + MusicDBTableNames.RecordCompanies + " rec " +
                "WHERE rec.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, ReadInfoFromMySqlDataReader);
        }

        /// <summary>
        /// Updates record company data from a MySqlDataReader object. The DataReader needs to be pointing to a data row. This method does not advance the reader, just gets the info
        /// from the current row.
        /// </summary>
        /// <param name="reader">MySqlDataReader object to read.</param>
        protected override void ReadInfoFromMySqlDataReader(MySqlDataReader reader)
        {
            SetID(reader["id"].ToString());
            Uri urlHome, urlFacebook;
            Name = reader["name"].ToString();
            Uri.TryCreate(reader["url_home"].ToString(), UriKind.Absolute, out urlHome);
            Uri.TryCreate(reader["url_facebook"].ToString(), UriKind.Absolute, out urlFacebook);
            UrlHome = urlHome;
            UrlFacebook = urlFacebook;
            UrlAlias = reader["urlalias"].ToString();
        }

        public List<Release> GetReleasesFromMySQLDB(string dbConnString)
        {
            Releases = new List<Release>();
            string cmdString = @"SELECT * FROM " + MusicDBTableNames.Releases +" rel " +
                "JOIN " + MusicDBTableNames.ReleasesAndRecordCompanies + " rar " +
                "WHERE rar.id_reccomp = @id_reccomp " +
                "ORDER BY rel.year";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id_reccomp", ID } }, AddReleaseToList);
            return Releases;
        }

        protected void AddReleaseToList(MySqlDataReader reader)
        {
            Releases.Add(new Release(reader));
        }

        public override void InsertToMySQLDB(string dbConnString)
        {
            // check if urlalias is available before inserting
            if (!CheckUrlAliasAvailability(UrlAlias, dbConnString))
                throw new UrlAliasNotAvailableException("recordcompany, urlalias: " + UrlAlias);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.RecordCompanies + "(name,url_home,url_facebook,urlalias) " +
                "VALUES (@name,@urlhome,@urlfacebook,@urlalias)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@urlhome", ((UrlHome is Uri) ? UrlHome.ToString() : ""));
                    cmd.Parameters.AddWithValue("@urlfacebook", ((UrlFacebook is Uri) ? UrlFacebook.ToString() : ""));
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when adding record company to DB");
                    ID = (int)cmd.LastInsertedId;
                    Console.WriteLine("Inserted record company to DB, ID: " + ID.ToString());
                }
                dbConnection.Close();
            }
        }

        public static string GenerateUrlAlias(string name)
        {
            name = name.ToLower();
            return Regex.Replace(name, @"[^A-Za-z0-9_\.~]+", "_");
        }

        protected override string GenerateUrlAlias()
        {
            return GenerateUrlAlias(Name);
        }

        /// <summary>
        /// Delete record company from DB.
        /// </summary>
        public override void DeleteFromMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }

        public override void UpdateMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }
    }
}
