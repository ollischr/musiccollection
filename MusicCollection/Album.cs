﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollection
{
    public class Album : MusicEntity
    {
        // TODO: input value checks to setters
        public Artist Artist { get; set; }      // TODO: support for multiple artists per album
        public string Title { get; set; }
        public Int16 Year { get; set; }
        public List<Person> Musicians { get; set; }
        public List<Person> Personnel { get; set; }
        public List<Release> Releases { get; set; }
        public bool VariousArtists { get; set; }

        public Album() : base() { }
        public Album(string id) : base(id) { }
        public Album(int id) : base(id) { }

        public Album(int id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Album(string id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Album(MySqlDataReader reader) : base()
        {
            ReadInfoFromMySqlDataReader(reader);
        }

        protected override void Setup()
        {
            DBTableName = MusicDBTableNames.Albums;
            Musicians = new List<Person>();
            Personnel = new List<Person>();
            Releases = new List<Release>();
        }

        /// <summary>
        /// Update album info from the DB. Separate methods exist for updating musician and personnel lists and the artist object.
        /// </summary>
        public override void ReadInfoFromMySQLDB(string dbConnString)
        {
            // read album info from MySQL DB
            string cmdString = @"SELECT alb.id, alb.id_artist, alb.title, alb.year_original, alb.urlalias
                    FROM " + MusicDBTableNames.Albums + " alb " +
                    "WHERE alb.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, ReadInfoFromMySqlDataReader);
        }

        /// <summary>
        /// Updates album data from a MySqlDataReader object. The DataReader needs to be pointing to a data row. This method does not advance the reader, just gets the info
        /// from the current row. Does not update the musician and personnel lists and the artist object.
        /// </summary>
        /// <param name="reader">MySqlDataReader object to read.</param>
        protected override void ReadInfoFromMySqlDataReader(MySqlDataReader reader)
        {
            SetID(reader["id"].ToString());
            Artist = new Artist(reader["id_artist"].ToString());
            Year = Int16.Parse(reader["year_original"].ToString());
            Title = reader["title"].ToString();
            UrlAlias = reader["urlalias"].ToString();
        }

        public void UpdateLineupFromMySQLDB(string dbConnString)
        {
            Musicians.Clear();

            // read lineup info from MySQL DB
            string cmdString = @"SELECT per.*
                    FROM " + MusicDBTableNames.Persons + " per " +
                    "JOIN " + MusicDBTableNames.AlbumsAndMusicians + " aam on aam.id_person = per.id " +
                    "JOIN " + MusicDBTableNames.Albums + " alb on alb.id = aam.id_album " +
                    "WHERE alb.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, AddMusicianToList);
        }

        protected void AddMusicianToList(MySqlDataReader reader)
        {
            Musicians.Add(new Person(reader));
        }

        public void UpdatePersonnelFromMySQLDB(string dbConnString)
        {
            Personnel.Clear();

            // read lineup info from MySQL DB
            string cmdString = @"SELECT per.*
                    FROM " + MusicDBTableNames.Persons + " per " +
                    "JOIN " + MusicDBTableNames.AlbumsAndPersonnel + " aap on aap.id_person = per.id " +
                    "JOIN " + MusicDBTableNames.Albums + " alb on alb.id = aap.id_album " +
                    "WHERE alb.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, AddPersonnelToList);
        }

        protected void AddPersonnelToList(MySqlDataReader reader)
        {
            Personnel.Add(new Person(reader));
        }

        /// <summary>
        /// Get all the releases of this album.
        /// </summary>
        /// <returns>The list of releases.</returns>
        public List<Release> GetReleasesFromMySQLDB(string dbConnString)
        {
            Releases = new List<Release>();
            string cmdString = @"SELECT *
                    FROM " + MusicDBTableNames.Releases + " rel " +
                    "WHERE rel.id_album = @id ORDER BY year";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, AddReleaseToList);
            return Releases;
        }

        protected void AddReleaseToList(MySqlDataReader reader)
        {
            Releases.Add(new Release(reader));
        }

        private void WriteMusicianInfoToDB(string dbConnString)
        {
            // clear previous entries for this album before inserting new
            RemoveMusicianInfoFromDB(dbConnString);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.AlbumsAndMusicians + "(id_album,id_person) " +
                "VALUES (@id_album,@id_person)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                foreach (Person musician in Musicians)
                {
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                    {
                        Console.WriteLine("album: " + ID.ToString() + ", musician: " + musician.ID.ToString());
                        cmd.Parameters.AddWithValue("@id_album", ID);
                        cmd.Parameters.AddWithValue("@id_person", musician.ID);
                        cmd.Prepare();
                        if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                            throw new MusicCollectionDBException("Something unexpected happened when adding album musician info to DB");
                    }
                }
                dbConnection.Close();
            }
            Console.WriteLine("WriteMusicianInfoToDB OK");
        }

        private void WritePersonnelInfoToDB(string dbConnString)
        {
            // clear previous entries for this album before inserting new
            RemovePersonnelInfoFromDB(dbConnString);
            
            string cmdString = @"INSERT INTO " + MusicDBTableNames.AlbumsAndPersonnel + "(id_album,id_person) " +
                "VALUES (@id_album,@id_person)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                foreach (Person person in Personnel)
                {
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                    {
                        cmd.Parameters.AddWithValue("@id_album", ID);
                        cmd.Parameters.AddWithValue("@id_person", person.ID);
                        cmd.Prepare();
                        if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                            throw new MusicCollectionDBException("Something unexpected happened when adding album personnel info to DB");
                    }
                }
                dbConnection.Close();
            }
            Console.WriteLine("WritePersonnelInfoToDB OK");
        }

        private void RemoveMusicianInfoFromDB(string dbConnString)
        {
            // TODO: make sure ID is set

            string cmdString = @"DELETE FROM " + MusicDBTableNames.AlbumsAndMusicians +
                " WHERE id_album = @id_album";
            Console.WriteLine(cmdString);
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@id_album", ID);
                    cmd.Prepare();
                    cmd.ExecuteNonQuery(); // return value = # of affected rows
                }
                dbConnection.Close();
            }
            Console.WriteLine("RemoveMusicianInfoFromDB OK");
        }

        private void RemovePersonnelInfoFromDB(string dbConnString)
        {
            // TODO: make sure ID is set

            string cmdString = @"DELETE FROM " + MusicDBTableNames.AlbumsAndPersonnel +
                " WHERE id_album = @id_album";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@id_album", ID);
                    cmd.Prepare();
                    cmd.ExecuteNonQuery(); // return value = # of affected rows
                }
                dbConnection.Close();
            }
            Console.WriteLine("RemovePersonnelInfoFromDB OK");
        }

        /// <summary>
        /// Adds album to database.
        /// </summary>
        /// <param name="dbConnString">MySQL connection string.</param>
        public override void InsertToMySQLDB(string dbConnString)
        {
            // TODO: album's dont have to have unique urlaliases or should they? fix this stuff when making web implementation
            // check if urlalias is available before inserting
            //if (!CheckUrlAliasAvailability(UrlAlias, dbConnString))
            //    throw new UrlAliasNotAvailableException("album, urlalias: " + UrlAlias);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.Albums + "(id_artist, title, year_original,urlalias) " +
                "VALUES (@id_artist,@title, @year,@urlalias)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@id_artist", Artist.ID);
                    cmd.Parameters.AddWithValue("@title", Title);
                    cmd.Parameters.AddWithValue("@year", Year);
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when adding album to DB");
                    ID = (int)cmd.LastInsertedId;
                }
                dbConnection.Close();
            }

            // update the albums_and_musicians and albums_and_personnel tables
            WriteMusicianInfoToDB(dbConnString);
            WritePersonnelInfoToDB(dbConnString);
        }

        public override void UpdateMySQLDB(string dbConnString)
        {
            string cmdString = @"UPDATE " + MusicDBTableNames.Albums + " SET " +
                "id_artist = @id_artist, " +
                "title = @title, " +
                "year_original = @year_original, " +
                "urlalias = @urlalias " +
                "WHERE id = @id";
            Console.WriteLine(cmdString);
            Console.WriteLine(Artist.ID.ToString() + " " + Title + " " + Year.ToString() + " " + UrlAlias + " " + ID.ToString());
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@id_artist", Artist.ID);
                    cmd.Parameters.AddWithValue("@title", Title);
                    cmd.Parameters.AddWithValue("@year_original", Year);
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Parameters.AddWithValue("@id", ID);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when updating album in DB");
                }
                dbConnection.Close();
            }

            // update the albums_and_musicians and albums_and_personnel tables
            WriteMusicianInfoToDB(dbConnString);
            WritePersonnelInfoToDB(dbConnString);
        }

        public static string GenerateUrlAlias(string albumTitle)
        {
            albumTitle = albumTitle.ToLower();
            return Regex.Replace(albumTitle, @"[^A-Za-z0-9_\.~]+", "_");
        }

        protected override string GenerateUrlAlias()
        {
            return GenerateUrlAlias(Title);
        }

        /// <summary>
        /// Delete album from DB.
        /// </summary>
        public override void DeleteFromMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }
    }
}
