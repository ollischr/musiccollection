﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollection
{
    /// <summary>
    /// A MusicCollection entity that has an ID and an urlalias (album, artist, release etc.)
    /// </summary>
    public abstract class MusicEntity
    {
        protected string _urlalias;

        public int ID { get; set; }
        public string Description { get; protected set; }
        public DateTime Created { get; protected set; }
        public DateTime Updated { get; protected set; }
        public virtual string UrlAlias { get; set; }
        protected string DBTableName { get; set; }

        public MusicEntity(int id)
        {
            Setup();
            ID = id;
        }
        public MusicEntity(string id)
        {
            Setup();
            SetID(id);
        }

        public MusicEntity()
        {
            Setup();
        }

        protected void SetID(string id) {
            int tempID;
            if (Int32.TryParse(id, out tempID))
                ID = tempID;
        }

        public override bool Equals(object obj)
        {
            // entities are considered equal if IDs are the same and they are of the same type
            if (obj == null || !(obj.GetType() == this.GetType()))
                return false;

            if (((MusicEntity)obj).ID == ID)
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 21 + this.GetType().GetHashCode();
            hash = hash * 21 + ID.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Checks if URL alias has already been taken in the DB.
        /// </summary>
        /// <param name="urlAlias">The URL alias to check.</param>
        /// <returns>True if available, false if not.</returns>
        public bool CheckUrlAliasAvailability(string urlAlias, string connString)
        {
            bool result = false;
            int count = 0;
            string cmdString = @"SELECT COUNT(*) FROM " + DBTableName + " WHERE urlalias = @urlalias";
            using (MySqlConnection dbConnection = new MySqlConnection(connString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@urlalias", urlAlias);
                    cmd.Prepare();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        count = int.Parse(reader.GetString(0));
                        if (count == 0)
                            result = true;
                        else if (count == 1)
                            result = false;
                        else if (count > 1)
                            result = false;     // TODO: throw exception or something, these values should be unique in the DB
                    }
                }
                dbConnection.Close();
            }
            Console.WriteLine("table: " + DBTableName + ", urlalias: " + urlAlias + ", count: " + count);
            return result;
        }
        protected abstract string GenerateUrlAlias();
        protected abstract void Setup();

        public abstract void InsertToMySQLDB(string dbConnString);
        public abstract void DeleteFromMySQLDB(string dbConnString);
        public abstract void ReadInfoFromMySQLDB(string dbConnString);
        protected abstract void ReadInfoFromMySqlDataReader(MySqlDataReader reader);
        public abstract void UpdateMySQLDB(string dbConnString);

        public delegate void ReaderResultDelegate(MySqlDataReader reader);

        /// <summary>
        /// Execute a MySQL SELECT query and do something for the DataReader rows
        /// </summary>
        /// <param name="dbConnString">DB connection string</param>
        /// <param name="queryString">SQL SELECT query string (prepared statement)</param>
        /// <param name="queryParams">Query parameters (as keys) and their values (as values)</param>
        /// <param name="resultDelegate">The method to be run for each row of the resulting MySqlDataReader object</param>
        public void ExecuteMySQLSelectQuery(string dbConnString, string queryString, Dictionary<string, object> queryParams, ReaderResultDelegate resultDelegate)
        {
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(queryString, dbConnection))
                {
                    foreach (string paramName in queryParams.Keys)
                        cmd.Parameters.AddWithValue("@" + paramName, queryParams[paramName]);
                    cmd.Prepare();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                        while (reader.Read())
                            resultDelegate(reader);
                }
                dbConnection.Close();
                Console.WriteLine("jepo " + resultDelegate.Method.Name);
            }
        }
    }
}
