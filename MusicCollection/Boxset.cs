﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using MusicCollection;

namespace MusicCollection
{
    public class Boxset : MusicEntity
    {
        // TODO: input value checks to setters
        public Artist Artist { get; set; }      // TODO: support for multiple artists per boxset
        public string Title { get; set; }
        public Int16 Year { get; set; }
        public List<RecordCompany> RecordCompanies { get; set; }

        public Boxset(int id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Boxset(string id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Boxset() : base() { }
        public Boxset(string id) : base(id) { }
        public Boxset(int id) : base(id) { }
        public Boxset(MySqlDataReader reader)
            : base()
        {
            ReadInfoFromMySqlDataReader(reader);
        }

        protected override void Setup()
        {
            DBTableName = MusicDBTableNames.Boxsets;
            RecordCompanies = new List<RecordCompany>();
        }

        public override void ReadInfoFromMySQLDB(string dbConnString)
        {
            // read album info from MySQL DB
            string cmdString = @"SELECT box.id, box.id_artist, box.title, box.year_original, box.urlalias
                    FROM " + MusicDBTableNames.Boxsets + " box " +
                    "WHERE box.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, ReadInfoFromMySqlDataReader);
        }

        /// <summary>
        /// Updates boxset data from a MySqlDataReader object. The DataReader needs to be pointing to a data row. This method does not advance the reader, just gets the info
        /// from the current row. The artist object and the record company list are not updated.
        /// </summary>
        /// <param name="reader">MySqlDataReader object to read.</param>
        protected override void ReadInfoFromMySqlDataReader(MySqlDataReader reader)
        {
            SetID(reader["id"].ToString());
            Artist = new Artist(reader["id_artist"].ToString());    // just a dummy object with the correct ID
            Year = Int16.Parse(reader["year_original"].ToString());
            Title = reader["title"].ToString();
            UrlAlias = reader["urlalias"].ToString();
        }

        public static string GenerateUrlAlias(string title)
        {
            title = title.ToLower();
            return Regex.Replace(title, @"[^A-Za-z0-9_\.~]+", "_");
        }

        protected override string GenerateUrlAlias()
        {
            return GenerateUrlAlias(Title);
        }

        public override void InsertToMySQLDB(string dbConnString)
        {
            // check if urlalias is available before inserting
            if (!CheckUrlAliasAvailability(UrlAlias, dbConnString))
                throw new UrlAliasNotAvailableException("boxset, urlalias: " + UrlAlias);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.Boxsets + "(title,id_artist,year_original,urlalias) " +
                "VALUES (@title,@id_artist,@year,@urlalias)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@title", Title);
                    cmd.Parameters.AddWithValue("@id_artist", Artist.ID);
                    cmd.Parameters.AddWithValue("@year", Year);
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when adding boxset to DB");
                    ID = (int)cmd.LastInsertedId;
                    Console.WriteLine("Inserted boxset to DB, ID: " + ID.ToString());
                }
                dbConnection.Close();
            }

            // update releases_and_recordcompanies
            cmdString = @"INSERT INTO " + MusicDBTableNames.ReleasesAndRecordCompanies + "(id_release,id_reccomp) VALUES (@id_release,@id_reccomp)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                foreach (RecordCompany reccomp in RecordCompanies)
                {
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                    {
                        cmd.Parameters.AddWithValue("@id_release", ID);
                        cmd.Parameters.AddWithValue("@id_reccomp", reccomp.ID);
                        cmd.Prepare();
                        if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                            throw new Exception("Something unexpected happened when adding boxset to DB");
                        Console.WriteLine("insert okei");
                    }
                }
                dbConnection.Close();
            }
        }

        /// <summary>
        /// Delete boxset from DB.
        /// </summary>
        public override void DeleteFromMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }

        public override void UpdateMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }
    }
}
