﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text.RegularExpressions;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollection
{
    public class Release : MusicEntity
    {
        #region Properties
        // TODO: input value checks to setters
        public Album Album { get; set; }
        public DateTime ReleaseDate { get; set; }
        public List<RecordCompany> RecordCompanies { get; set; }
        public MediaUnitSet MediaUnits { get; set; }
        public Boxset Boxset { get; set; }
        public bool Reissue { get; set; }
        public bool Remastered { get; set; }
        public bool LimitedEdition { get; set; }
        public Int16 Year { get; set; }
        public string Title { get; set; }

        public string DisplayString {
            get {
                return Year.ToString() + " " + MediaUnits.ToString();
            }
        }
        #endregion

        #region Constructors
        public Release() { }
        public Release(int id) : base(id) { }
        public Release(int id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }
        public Release(string id) { }
        public Release(string id, string dbConnString)
            : base(id)

        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Release(MySqlDataReader reader)
            : base()
        {
            ReadInfoFromMySqlDataReader(reader);
        }
        #endregion

        protected override void Setup()
        {
            DBTableName = MusicDBTableNames.Releases;
            RecordCompanies = new List<RecordCompany>();
            MediaUnits = new MediaUnitSet();
        }

        public static string GenerateUrlAlias(string releaseTitle, string albumTitle)
        {
            releaseTitle = releaseTitle.ToLower();
            albumTitle = albumTitle.ToLower();
            return (Regex.Replace(albumTitle, @"[^A-Za-z0-9_\.~]+", "_") + "_" + Regex.Replace(releaseTitle, @"[^A-Za-z0-9_\.~]+", "_")).TrimEnd('_');
        }

        protected override string GenerateUrlAlias()
        {
            return GenerateUrlAlias(Title, Album.Title);
        }

        public override void ReadInfoFromMySQLDB(string dbConnString)
        {
            // read album info from MySQL DB
            string cmdString = @"SELECT rel.id, rel.id_album, rel.year, rel.id_boxset, rel.reissue, rel.remastered, rel.limited_edition, rel.mediaunits, rel.title_additional, rel.urlalias
                    FROM " + MusicDBTableNames.Releases + " rel " +
                    "WHERE rel.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, ReadInfoFromMySqlDataReader);
        }

        /// <summary>
        /// Updates release data from a MySqlDataReader object. The DataReader needs to be pointing to a data row. This method does not advance the reader, just gets the info
        /// from the current row. Does not update the album object and the boxset object.
        /// </summary>
        /// <param name="reader">MySqlDataReader object to read.</param>
        protected override void ReadInfoFromMySqlDataReader(MySqlDataReader reader)
        {
            SetID(reader["id"].ToString());
            Album = new Album(reader["id_album"].ToString());
            Year = Int16.Parse(reader["year"].ToString());
            Title = reader["title_additional"].ToString();
            if (!reader.IsDBNull(reader.GetOrdinal("id_boxset")))   // set boxset if value is not null
                Boxset = new Boxset(reader.GetInt32("id_boxset"));
            Reissue = reader.GetBoolean("reissue");
            Remastered = reader.GetBoolean("remastered");
            LimitedEdition = reader.GetBoolean("limited_edition");
            foreach (string mediaUnit in reader["mediaunits"].ToString().Split('+'))
            {
                int count = 1;
                string mediaUnitTrimmed = mediaUnit;
                Match mats = Regex.Match(mediaUnit, @"(\d+)([A-Z]+)");
                if (mats.Success)
                {
                    count = int.Parse(mats.Groups[1].Value);
                    mediaUnitTrimmed = mats.Groups[2].Value;
                }
                for (int i = 0; i < count; i++)
                    MediaUnits.Units.Add((MediaUnitType)Enum.Parse(typeof(MediaUnitType), mediaUnitTrimmed));
            }
            UrlAlias = reader["urlalias"].ToString();
        }

        public override void InsertToMySQLDB(string dbConnString)
        {
            // check if urlalias is available before inserting
            if (!CheckUrlAliasAvailability(UrlAlias, dbConnString))
                throw new UrlAliasNotAvailableException("release, urlalias: " + UrlAlias);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.Releases + "(id_album,id_boxset,reissue,remastered,limited_edition,title_additional,urlalias,year,mediaunits) " +
                "VALUES (@id_album,@id_boxset,@reissue,@remastered,@limited,@title,@urlalias,@year,@mediaunits)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@id_album", Album.ID);
                    cmd.Parameters.AddWithValue("@id_boxset", (Boxset is Boxset) ? Boxset.ID.ToString() : null);
                    cmd.Parameters.AddWithValue("@reissue", Reissue);
                    cmd.Parameters.AddWithValue("@remastered", Remastered);
                    cmd.Parameters.AddWithValue("@limited", LimitedEdition);
                    cmd.Parameters.AddWithValue("@title", Title);
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Parameters.AddWithValue("@year", Year);
                    cmd.Parameters.AddWithValue("@mediaunits", MediaUnits.ToString());
                    
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when adding release to DB");
                    ID = (int)cmd.LastInsertedId;
                    Console.WriteLine("Inserted releases to DB, ID: " + ID.ToString());
                }
                dbConnection.Close();
            }

            // update releases_and_recordcompanies
            cmdString = @"INSERT INTO " + MusicDBTableNames.ReleasesAndRecordCompanies + "(id_release,id_reccomp) VALUES (@id_release,@id_reccomp)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                foreach (RecordCompany reccomp in RecordCompanies)
                {
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                    {
                        cmd.Parameters.AddWithValue("@id_release", ID);
                        cmd.Parameters.AddWithValue("@id_reccomp", reccomp.ID);
                        cmd.Prepare();
                        if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                            throw new Exception("Something unexpected happened when adding release to DB");
                        Console.WriteLine("insert okei");
                    }
                }
                dbConnection.Close();
            }
        }

        /// <summary>
        /// Delete release from DB.
        /// </summary>
        public override void DeleteFromMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }

        public override void UpdateMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }
    }
}
