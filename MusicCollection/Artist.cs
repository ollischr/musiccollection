﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace MusicCollection
{
    public class Artist : MusicEntity
    {
        // TODO: input value checks to setters
        public string Name { get; set; }
        public Uri UrlHome { get; set; }
        public Uri UrlWikipedia { get; set; }
        public Uri UrlRateYourMusic { get; set; }
        public Uri UrlFacebook { get; set; }

        public Artist(int id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Artist(string id, string dbConnString)
            : base(id)
        {
            // TODO: check connection string validity
            ReadInfoFromMySQLDB(dbConnString);
        }

        public Artist() : base() { }
        public Artist(int id) : base(id) { }
        public Artist(string id) : base(id) { }
        public Artist(MySqlDataReader reader) : base()
        {
            ReadInfoFromMySqlDataReader(reader);
        }

        protected List<Boxset> Boxsets { get; set; }
        protected List<Person> Musicians { get; set; }
        protected List<Person> Personnel { get; set; }

        protected override void Setup()
        {
            DBTableName = MusicDBTableNames.Artists;
            Boxsets = new List<Boxset>();
            Musicians = new List<Person>();
            Personnel = new List<Person>();
        }

        public override void ReadInfoFromMySQLDB(string dbConnString)
        {
            // read artist info from MySQL DB
            string cmdString = @"SELECT art.id, art.name, art.url_home, art.url_wiki, art.url_rym, art.urlalias
                FROM " + MusicDBTableNames.Artists + " art " +
                "WHERE art.id = @id";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id", ID } }, ReadInfoFromMySqlDataReader);
        }

        /// <summary>
        /// Updates artist data from a MySqlDataReader object. The DataReader needs to be pointing to a data row. This method does not advance the reader, just gets the info
        /// from the current row.
        /// </summary>
        /// <param name="reader">MySqlDataReader object to read.</param>
        protected override void ReadInfoFromMySqlDataReader(MySqlDataReader reader)
        {
            SetID(reader["id"].ToString());
            Uri urlHome, urlWikipedia, urlRateYourMusic;
            Name = reader["name"].ToString();
            Uri.TryCreate(reader["url_home"].ToString(), UriKind.Absolute, out urlHome);
            Uri.TryCreate(reader["url_wiki"].ToString(), UriKind.Absolute, out urlWikipedia);
            Uri.TryCreate(reader["url_rym"].ToString(), UriKind.Absolute, out urlRateYourMusic);
            UrlHome = urlHome;
            UrlWikipedia = urlWikipedia;
            UrlRateYourMusic = urlRateYourMusic;
            UrlAlias = reader["urlalias"].ToString();
        }

        /// <summary>
        /// Get albums by this artist.
        /// </summary>
        /// <param name="dbConnString">MySQL DB connection string.</param>
        /// <returns>List of albums.</returns>
        public List<Album> GetAlbumsFromMySQLDB(string dbConnString)
        {
            return MusicCollection.GetAlbumsFromMySQLDB(dbConnString, new List<Artist> { this });
        }

        /// <summary>
        /// Get boxsets by this artist.
        /// </summary>
        /// <param name="dbConnString">MySQL DB connection string.</param>
        /// <returns>List of boxsets.</returns>
        public List<Boxset> GetBoxsetsFromMySQLDB(string dbConnString)
        {
            // read list of boxsets from MySQL DB
            Boxsets.Clear();
            string cmdString = @"SELECT * FROM " + MusicDBTableNames.Boxsets + " " +
                "WHERE id_artist = @id_artist " +
                "ORDER BY year_original";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id_artist", ID } }, AddBoxsetToList);
            return Boxsets;
        }

        protected void AddBoxsetToList(MySqlDataReader reader) {
            Boxsets.Add(new Boxset(reader));
        }

        /// <summary>
        /// Get list of musicians that are on albums by this artist.
        /// </summary>
        /// <param name="dbConnString">The DB connection string.</param>
        /// <returns>List of musicians.</returns>
        public List<Person> GetMusiciansFromMySQLDB(string dbConnString)
        {
            // read list of musicians from MySQL DB
            Musicians.Clear();
            string cmdString = @"SELECT DISTINCT per.* FROM " + MusicDBTableNames.Persons + " per " +
                "JOIN " + MusicDBTableNames.AlbumsAndMusicians + " aam ON per.id = aam.id_person " +
                "JOIN " + MusicDBTableNames.Albums + " alb ON alb.id = aam.id_album " +
                "JOIN " + MusicDBTableNames.Artists + " art ON art.id = alb.id_artist " +
                "WHERE id_artist = @id_artist " +
                "ORDER BY per.surname";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id_artist", ID } }, AddMusicianToList);
            return Musicians;
        }

        protected void AddMusicianToList(MySqlDataReader reader)
        {
            Musicians.Add(new Person(reader));
        }

        /// <summary>
        /// Get list of personnel that are on albums by this artist.
        /// </summary>
        /// <param name="dbConnString">The DB connection string.</param>
        /// <returns>List of personnel.</returns>
        public List<Person> GetPersonnelFromMySQLDB(string dbConnString)
        {
            // read list of personnel from MySQL DB
            Personnel.Clear();
            string cmdString = @"SELECT DISTINCT per.* FROM " + MusicDBTableNames.Persons + " per " +
                "JOIN " + MusicDBTableNames.AlbumsAndPersonnel + " aap ON per.id = aap.id_person " +
                "JOIN " + MusicDBTableNames.Albums + " alb ON alb.id = aap.id_album " +
                "JOIN " + MusicDBTableNames.Artists + " art ON art.id = alb.id_artist " +
                "WHERE id_artist = @id_artist " +
                "ORDER BY per.surname";
            ExecuteMySQLSelectQuery(dbConnString, cmdString, new Dictionary<string, object> { { "id_artist", ID } }, AddPersonnelToList);
            return Musicians;
        }

        protected void AddPersonnelToList(MySqlDataReader reader)
        {
            Personnel.Add(new Person(reader));
        }

        public static string GenerateUrlAlias(string name)
        {
            name = name.ToLower();
            return Regex.Replace(name, @"[^A-Za-z0-9_\.~]+", "_");
        }

        protected override string GenerateUrlAlias()
        {
            return GenerateUrlAlias(Name);
        }

        public override void InsertToMySQLDB(string dbConnString)
        {
            // check if urlalias is available before inserting
            if (!CheckUrlAliasAvailability(UrlAlias, dbConnString))
                throw new UrlAliasNotAvailableException("artist, urlalias: " + UrlAlias);

            string cmdString = @"INSERT INTO " + MusicDBTableNames.Artists + "(name,url_home,url_rym,url_wiki,url_facebook,urlalias) " +
                "VALUES (@name,@urlhome,@urlrym,@urlwiki,@urlfacebook,@urlalias)";
            using (MySqlConnection dbConnection = new MySqlConnection(dbConnString))
            {
                dbConnection.Open();
                using (MySqlCommand cmd = new MySqlCommand(cmdString, dbConnection))
                {
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@urlhome", ((UrlHome is Uri) ? UrlHome.ToString() : ""));
                    cmd.Parameters.AddWithValue("@urlrym", ((UrlRateYourMusic is Uri) ? UrlRateYourMusic.ToString() : ""));
                    cmd.Parameters.AddWithValue("@urlwiki", ((UrlWikipedia is Uri) ? UrlWikipedia.ToString() : ""));
                    cmd.Parameters.AddWithValue("@urlfacebook", ((UrlFacebook is Uri) ? UrlFacebook.ToString() : ""));
                    cmd.Parameters.AddWithValue("@urlalias", UrlAlias);
                    cmd.Prepare();
                    if (cmd.ExecuteNonQuery() != 1) // return value = # of affected rows
                        throw new MusicCollectionDBException("Something unexpected happened when adding artist to DB");
                    ID = (int)cmd.LastInsertedId;
                    Console.WriteLine("Inserted artist to DB, ID: " + ID.ToString());
                }
                dbConnection.Close();
            }
        }

        /// <summary>
        /// Delete artist from DB.
        /// </summary>
        public override void DeleteFromMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }

        public override void UpdateMySQLDB(string dbConnString)
        {
            throw new NotImplementedException();
        }
    }
}
