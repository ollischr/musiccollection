### What is this repository for? ###

A GUI and the backend library for managing a record collection, written in C# and using the .NET framework and a MySQL/MariaDB database. Ultimately the user will be able to insert, delete, update and browse entries in the DB using the graphical interface.

Developed and tested with Visual Studio in Windows. A MonoDevelop/Linux version may come in the future.

### How do I get set up? ###
Requirements:

* Windows
* .NET 4.5 framework
* MySQL/MariaDB database (.sql file is included)

### Contribution guidelines ###

Not taking contributions at the moment.

### Future plans ###

* Better documentation
* More thoroughly commented code
* Unit tests
* Functionality for deleting data from DB
* Functionality for updating data in DB
* Optimizing MySQL queries during operation
* Linux support
* Web interface
* Support for images (album cover art etc.)
* Implement GUI using something like WPF or even HTML5 instead of WinForms
* Installer

### Who do I talk to? ###

Olli Schroderus