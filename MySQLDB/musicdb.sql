-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for musiccollection
CREATE DATABASE IF NOT EXISTS `musiccollection` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `musiccollection`;


-- Dumping structure for table musiccollection.albums
CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_artist` int(10) unsigned DEFAULT NULL,
  `title` tinytext,
  `comments` text,
  `muokattu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `urlalias` tinytext,
  `year_original` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.albums_and_musicians
CREATE TABLE IF NOT EXISTS `albums_and_musicians` (
  `id_album` int(5) NOT NULL,
  `id_person` int(5) NOT NULL,
  `instrument` tinytext,
  `status` enum('member','guest') DEFAULT NULL,
  PRIMARY KEY (`id_person`,`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.albums_and_personnel
CREATE TABLE IF NOT EXISTS `albums_and_personnel` (
  `id_album` int(5) NOT NULL,
  `id_person` int(5) NOT NULL,
  `role` set('producer','engineer','cover_artist') DEFAULT NULL,
  PRIMARY KEY (`id_person`,`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.artists
CREATE TABLE IF NOT EXISTS `artists` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` text,
  `url_home` text,
  `url_wiki` text,
  `url_rym` text,
  `url_facebook` text,
  `urlalias` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.boxsets
CREATE TABLE IF NOT EXISTS `boxsets` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_artist` int(10) unsigned DEFAULT NULL,
  `title` tinytext,
  `year_original` year(4) DEFAULT NULL,
  `comments` text,
  `reccomp` tinytext,
  `muokattu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `urlalias` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.file_paths
CREATE TABLE IF NOT EXISTS `file_paths` (
  `name` enum('album_covers','other') NOT NULL DEFAULT 'other',
  `path` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.persons
CREATE TABLE IF NOT EXISTS `persons` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `firstname` tinytext,
  `surname` tinytext,
  `birthdate` date DEFAULT NULL,
  `deathdate` date DEFAULT NULL,
  `urlalias` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.recordcompanies
CREATE TABLE IF NOT EXISTS `recordcompanies` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `url_home` text,
  `url_facebook` text,
  `urlalias` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.releases
CREATE TABLE IF NOT EXISTS `releases` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_album` int(5) NOT NULL DEFAULT '0',
  `reissue` bit(1) NOT NULL DEFAULT b'0',
  `id_boxset` int(5) DEFAULT NULL,
  `urlalias` tinytext,
  `title_additional` tinytext,
  `remastered` bit(1) NOT NULL DEFAULT b'0',
  `limited_edition` bit(1) NOT NULL DEFAULT b'0',
  `year` year(4) NOT NULL DEFAULT '2000',
  `mediaunits` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table musiccollection.releases_and_recordcompanies
CREATE TABLE IF NOT EXISTS `releases_and_recordcompanies` (
  `id_release` int(5) NOT NULL,
  `id_reccomp` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_reccomp`,`id_release`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
